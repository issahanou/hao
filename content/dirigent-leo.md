## De Dirigent

Leo van Lierop (1991) studeerde Accordeon bij Ronald van Overbruggen en Gerie Daanen en HaFa-
Directie bij Hardy Mertens aan het Fontys Conservatorium te Tilburg, met piano en orgel als bijvakken.
In 2015 rondde hij beide opleidingen cum laude af. Voor directie bezit hij de Bachelorgraad, 
voor accordeon het Masterdiploma.

<img src="/images/algemeen/dirigent-leo2.jpg" alt="dirigent" class="content-image content-image--right"/>

Gedurende zijn studie heeft Leo diverse masterclasses gehad van beroemde musici. Masterclasses
accordeon kreeg hij van o.a. Stefan Hussong, Friedrich Lips, Yuri Shishkin, Alexander Dmitriev en
Miljan Bjeletic. Verdieping in het uitvoeren van Barokmuziek kreeg hij bij klavecinist Jacques Ogg
en traversist Wilbert Hazelzet. Voor zijn specialisatie in het dirigeren van accordeonorkesten kreeg
hij les van de vermaarde dirigent Stefan Hippe in Nürnberg.

Als docerend en uitvoerend musicus reist hij het hele land af. Hij was meerdere malen prijswinnaar
bij het Nationaal Accordeon Concours en soleerde met verschillende harmonie-, symfonie- en
accordeonorkesten. Als accordeonist is hij actief in diverse samenstellingen. Naast zijn activiteiten
als solist vormt hij een duo met fluitiste Arjanne Bruggeman, speelt hij in Sexteto Recuerdo, dat
moderne tangomuziek naar de concertzalen brengt en is hij de accordeonist van het Brecht Trio,
dat samen met zangeres Kikki Vanhautem en pianiste Tessa Verboven muziek verzorgt rondom
schrijver Bertolt Brecht, met uitstapjes naar jazzmuziek uit de jaren ’30 t/m ’60.
Leo's repertoire omspant diverse stijlen; klassieke en moderne muziek voor solo accordeon, maar
ook vele werken voor accordeon in kamermuziekverband en grote werken met koor of orkest.

Tevens is hij een van de toonaangevende personen binnen de wereld van het accordeonorkest. Hij
is aangesteld als dirigent van Accordeonata Lekkerkerk, Accordeonvereniging Kunst Na Arbeid Epe,
Vita Accordeonistica ’83 uit Heerhugowaard en het door hemzelf opgerichte accordeonorkest
Veloce uit Zoetermeer. Met het laatstgenoemde orkest won hij in 2018 de programmaprijs op het
NOVAM Orkestenfestival in Ede.
Sinds september 2020 is Leo dirigent van het Haarlems Accordeonorkest.