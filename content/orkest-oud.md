# Het Orkest

Het Haarlems Accordeonorkest heeft op dit moment 27 leden (jongvolwassenen en volwassenen)
en behoort tot één van de beste accordeonorkesten van Nederland. 
Veel van de huidige spelers van het HAO hebben hun opleiding genoten bij Hart Muziekschool Haarlem 
(voorheen muziekcentrum Zuid Kennemerland), waardoor het een groot en bijzonder orkest is geworden.

Het orkest heeft de afgelopen jaren succesvol met verschillende koren en orkesten samengespeeld,
zo waren er concerten met het koor New Spirit, het Promenade Orkest, het Clarinet Choir en Swingkoor Stardust.

#### Naamsverandering

Het orkest is opgericht als Accordeonorkest 'Esmeralda'. In 2014 heeft het orkest besloten haar naam te veranderen in
‘Haarlems Accordeonorkest’. Het orkest heeft de afgelopen jaren een enorme groei doorgemaakt
en vond dat de oude naam niet meer paste bij de moderne en professionelere uitstraling 
en identiteit van het orkest. Met de nieuwe naam wil het orkest ook de duidelijke link 
met Haarlem benadrukken, waar de oorsprong van het orkest ligt.

Toenmalig dirigent Evert van Amsterdam: “Dit accordeonorkest is al jaren een begrip in de 
Nederlandse accordeonwereld. Het orkest onderscheidt zich door het programmeren van een 
grote verscheidenheid aan muziekstijlen. Ik kijk met plezier terug op de afgelopen jaren.
De nieuwe naam ‘Haarlems Accordeonorkest’ past beter bij de uitstraling van het huidige orkest.”

#### Repertoire

Het huidige repertoire van het orkest omvat een grote diversiteit aan muziekstijlen.
Naast werken in het klassieke genre (zoals Concerto Barocco van G. Mohr, 
Prealudium en Fuga van M. Seiber, Accordeonconcert van H. Herman) worden ook composities 
gespeeld vanuit de volks- en swingmuziek (Slavonska Rhapsodia van A. Götz, Suite van R. Bruci,
Zigeunerweise van P. de Sarasate, Music of Resurrection van W. Kahl).
Daarnaast omvat het repertoire van het orkest ook werken uit de film-, musical- en popmuziek
(Music van J. Miles, Pirates of the Caribbean).

<nuxt-link to="/orkest/repertoire">
    Voor een volledig overzicht van het repertoire, kijk hier.
</nuxt-link>

#### Hoogtepunten

Een aantal hoogtepunten van het orkest van de afgelopen jaren waren o.a. de presentatie van de eerste CD “Impressies” (2003)
en het Nationaal Accordeon Concours te Goor in 2005 (promotie eredivisie en wisselbeker). 
In 2008 werd opgetreden tijdens het Muziekfestival op de Grote Markt in Haarlem. Tevens werd er wederom succesvol deelgenomen
aan het nationaal orkestenconcours van de NOVAM in Hengelo.
Op 25 januari 2009 vond de viering plaats van het 50-jarig jubileum van het orkest in een uitverkochte zaal
in de Philharmonie te Haarlem. Tijdens dit concert werd oprichter Hans van den Bogaard benoemd tot erelid van het orkest.

Andere hoogtepunten van het HAO waren concertreizen naar Osnabrück en Dinslaken en werd er driemaal deelgenomen 
aan het Internationale Accordeonorkestenconcours in Innsbruck, waarin het orkest in 2013 een uitstekend resultaat behaalde 
in de hoogste afdeling: ‘Hochstufe’. Op dit concours de werken Sinfonietta Dramatica en Balkan Impressionen ten gehore gebracht.

In de Haarlemse Philharmonie werd in 2012 in een uitverkochte zaal de tweede cd “Promenade” gepresenteerd, 
daarnaast trad het orkest zeer succesvol op tijdens het REL-festival in Lelystad waar het HAO de publieksprijs kreeg.
In 2017 en 2018 behaalde het orkest een 1e prijs in de hoogste afdeling van het Nationaal Accordeonfestival van de Novam
in Ede. In het jubileumjaar 2019 is het orkest wederom afgereisd naar Innsbruck voor deelname aan het 
Internationaal Accordeonorkestenconcours. En werden in januari en november jubileumconcerten gegeven waar oude 
en nieuwe werken ten gehore werden gebracht. Het stuk 'Sometimes'werd speciaal voor dit jubileumjaar gecomponeerd
door Ad Wammes.
