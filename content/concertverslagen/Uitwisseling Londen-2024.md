---
title: Uitwisseling Londen 2024
description: Een concertreis naar Londen om een concert te geven met London Accordion Orchestra!
image: /images/agenda/HAO_en_LAO.jpeg
author: Ron
alt: Londen
date: 05 t/m 8 april 2024
date-order: 2024-04-05
---

Reisverslag London – London Accordeon Orchestra (LAO) 5-8 april 2024

Op vrijdag 5 april was het zover! Na intensieve voorbereiding vertrekken we naar Londen. De dag ervoor hebben we de instrumenten al bij Nina gebracht. Nina, Maaike en Priy vertrekken vrijdag met een grote bus gevuld met alle instrumenten naar Hoek van Holland, en nemen daar de boot naar Harwich om naar Londen te rijden. 
De rest van het orkest reist vrijdagmiddag met de Eurostar vanaf Amsterdam of Rotterdam naar Londen.  In Amsterdam is het vertrek nog even spannend. Bij de Britse Douane vraagt men wat wij in Londen gaan doen: Een concert geven… Toen kwamen er vraagtekens op hun hoofd….als we daar een performance gaan geven dan moeten we misschien wel Belasting formulieren hebben…. Nadat Leo als laatste door de security  was, kon hij met veel toelichting en het tonen van e-mails aannemelijk maken dat we een amateurorkest zijn dat NIET betaald wordt en op uitnodiging van het Londons Accordeon Orkest een bijdrage aan hun concert zal geven.  Na ruim een kwartier wachten (het leek wel een uur), konden we de trein in. Na een zeer voorspoedige reis arriveerden we keurig op tijd in Londen, waarna we met de metro en het laatste stukje te voet bij het Hotel aankwamen. De kamers werden verdeeld en degene die zin hadden gingen om de hoek nog wat drinken bij de Pub. Vlak voor sluitingstijd hebben we nog een drankje meegenomen voor de helden van het busje, waren ze erg blij mee! 

Trouwens erg leuk dat een aantal oud-leden, een bevriende “inval-slagwerker” en een aspirant lid mee zijn naar Londen. We hebben 4 repetities en een extra repetitiemiddag met de extra leden samen geoefend. 

Zaterdag wacht ons een zeer eenvoudig Engels ontbijt: Tosti Kaas of een bakje yoghurt, fruit en thee of oploskoffie. Nadat de ontbijtgasten de eetzaal verlaten hebben, kunen we als orkest die ruimte gebruiken voor een repetitie. Met een vrij laag plafond en plavuizen vloer, is het één grote klankkast. Dus spelen op halve kracht of nog minder, anders loopt iedereen de rest van de dag met knetterende oren! ’s Middags zijn we vrij totdat we eind van de middag een gezamenlijke borrel hebben met het LAO. Na de borrel is er met een deel van het LAO een gezamenlijk diner bij een lekker Pizzarestaurant. Fijn om het orkest te ontmoeten en op informele wijze kennis te maken. Na afloop nog een afzakkertje bij de lokale Pub, waarbij het nog even wennen is dat de Pub om 23.00 uur sluit en al ruim daarvoor een laatste ronde doet….

Zondag is de grote dag van het gezamenlijk concert. Om 09.45 uur mag eerst het Haarlems Accordeonorkest repeteren in de Tabernacle in London, daarna het London Accordion Orchestra. De stoelen voor het orkest zijn behoorlijk laag, maar met een kussentje of met een eigen kruk is het goed te doen. Om 14.00 uur geven we gezamenlijk een zeer goed bezocht concert, er zijn zelfs enkele laatkomers die moeten staan! Na alle voorbereidingen is het een heel fijn gevoel als het een zeer muzikaal concert is met mooie muziek. De orkesten vullen elkaar goed aan en zijn beide van hoog niveau! 
Na afloop drinken we een drankje en brengen de instrumenten naar het hotel. Er wordt samen met HAO en LAO in een vrije setting bij de Duke of Wellington nog wat gedronken en eten besteld. Er wordt gezellig nagepraat, ervaringen gedeeld en afscheid genomen. ’s Avonds volgt voor de liefhebber nog een bezoekje aan de Pub, en opnieuw worden wij verrast doordat iets na 22.00 uur al de laatste ronde wordt opgenomen. Het vervolg in een cocktail bar eindigt ook rond middernacht omdat de bar gaat sluiten. 

Maandag hebben we vrij programma en kunnen naar eigen wens de stad verkennen. De bekende hotspots zoals Big Ben, Westminster Abbey, Buckingham Palace en de Tower Bridge worden bezocht. Eind van de middag vertrekken de meesten weer met de Eurostar naar huis, de toppers van het busje hebben een late boot vanuit Harwich naar Hoek van Holland.

Al met al een geweldig leuk, gezellig en muzikaal avontuur!  
