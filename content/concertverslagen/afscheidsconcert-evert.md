---
title: Afscheidsconcert Evert van Amsterdam
description: Op 19 september heeft dirigent Evert van Amsterdam afscheid van ons genomen, na 22 jaar het orkest onder zijn leiding te hebben gehad. Het was een mooi concert in de Oosterkerk in Haarlem, met veel oud-leden aanwezig.
image: /images/fotoboeken/2020-afscheid-evert/DSC08969-small.JPG
author: Joëlle
alt: afscheid Evert
date: 21 september 2020
date-order: 2020-09-21
---
Op zaterdag 19 september vond het afscheidsconcert plaats van dirigent Evert van Amsterdam bij het Haarlems Accordeonorkest. Tot kort vantevoren bleef het spannend of dit concert wel door kon gaan. Op vrijdag werd nog een persconferentie gegeven waarin strengere maatregelen werden afgekondigd voor de regio, maar die gingen gelukkig pas vanaf zondagavond in. Dus kon het concert zoals gepland doorgang vinden. Het concert vond plaats in de Oosterkerk in Haarlem, waar normaal gesproken 300 bezoekers ontvangen kunnen worden, maar nu 80 mensen op 1,5 meter van elkaar konden plaatsnemen. Het was dus een ietwat besloten concert.

<img class="content-image content-image--right" src="/images/fotoboeken/2020-afscheid-evert/DSC08931-small.JPG"/>

Het orkest opende het concert met 3 delen uit de Holberg suite van Grieg en de wervelende Spanischer Tanz van Tsjaikovski. Na deze twee stukken nam Evert van Amsterdam het woord; hij bedankte het orkest voor de jarenlange prettige samenwerking. Veel orkestleden zijn in hun jonge jaren begonnen met accordeonles bij Evert op de muziekschool in Haarlem en kennen Evert dus al zolang ze zich herinneren. Het zal gek zijn om elkaar niet meer wekelijks te zien, maar uiteraard zal Evert het orkest op de voet blijven volgen.

Na Evert zijn speech volgden de delen 7 t/m 10 uit de Schilderijententoonstelling van Moessorgski. Deze delen werden vergezeld van een powerpoint presentatie waarin een toelichting stond op de schilderijen, zodat luisteraars de muziek beter konden plaatsen. Vervolgens werd het swingende Conga del Fuego Nuevo uitgevoerd. Dit stuk hadden wij in mei 2019 in Innsbruck gehoord door een Spaans orkest en alle orkestleden waren direct enthousiast om dit zelf ook te gaan spelen.
Er werd afgesloten met het stuk Sometimes dat speciaal voor ons jubileumjaar 2019 gecomponeerd is door Ad Wammes. De namen van de delen waren zeer toepasselijk bij de kernboodschap van deze middag namelijk: Sometimes, times were good. Sometimes, times were sad. But most of the time, times were great!

<img class="content-image content-image--left" src="/images/fotoboeken/2020-afscheid-evert/DSC08872-small.JPG"/>

Na dit muzikale deel werd Evert uitgebreid in het zonnetje gezet en werden vele memorabele momenten aangehaald door voorzitter Ron Paeper. In 22 jaar is er veel gebeurd, zowel in muzikaal als in sociaal opzicht. Zo zijn we drie keer afgereisd naar Innsbruck en hebben een aantal keer succesvol deelgenomen aan het nationale orkestenconcours. Ook werden verschillende cd's opgenomen. Er waren memorabele uitwisselingen met andere muziekgezelschappen, zoals koren, solisten, ensembles en andere accordeonorkesten. Het orkest is flink verjongd in die jaren met talent van de muziekschool en er is van repetitie-avond gewisseld om te zorgen dat in andere steden studerende leden ook konden blijven spelen bij het orkest. 

<img class="content-image content-image--right" src="/images/fotoboeken/2020-afscheid-evert/EvertVanAmsterdamAfscheidsconcert-102-small.jpg"/>
<br>
Evert kreeg verschillende kado's als herinnering aan deze mooie tijd. Als kers op de taart en complete verrassing voor Evert werd vervolgens door een octet, geheel bestaand uit (oud)orkestleden en oud-leerlingen van Evert het stuk Metropolitan uitgevoerd. Dit stuk was voor deze gelegenheid gearrangeerd door Roelof Ruis. Aan het eind van het stuk voegde de rest van het orkest zich onder leiding van nieuwe dirigent Leo van Lierop bij het octet om gezamenlijk de grote waardering voor Evert’s jarenlange inzet voor het orkest te laten blijken!

Joëlle Dek	
	
	
