---
title: MHM Festival Hoofddorp
description: Wij waren uitgenodigd om te komen spelen op het MHM Festival in Hoofddorp, ondanks de hete temperaturen vonden wij het leuk om hier te kunnn spelen.
image: /images/agenda/festival_hoofddorp2023.jpeg
author: Simone Visser
alt: festival
date: 25 juni 2023
date-order: 2023-06-25
---
Op 25 juni 2023 was het tijd om op het Muziekfestijn Haarlemmermeer & Meer (MHM) in Hoofddorp te spelen. Op het burgemeester van Stamplein was een zomermarkt gaande met een podium voor amateurorkesten. Het was erg warm, maar op het overdekte podium konden we onze muziek mooi ten gehore brengen. 

We begonnen met een energieke opening. Bij de muziek van Magnificent Seven kon je in gedachte de huurlingen op hun paarden door het Wilde Westen zien rijden. Daarna speelden we een medley van Supertramp met muziek die voor vele bekend is zoals: ‘logical song’ en ‘dreamer’. Na Supertramp was het tijd voor ‘de traan van Maxima’. Issa Hanou speelde de prachtige solo Adiós Nonino waarbij het orkest haar begeleidde. Hierna was het tijd voor film- en musicalmuziek: West Side Story stond op het programma. Dit was ook een medley waarin onder andere ‘I feel pretty’, ‘Maria’, ‘Tonight’ en ‘America’ voorbij kwamen. We eindigden met Puszta: Two Gipsy Tunes. Hier gleden onze vingers zo ongeveer van het klavier af van de warmte en de snelheid, maar dat mocht de pret niet drukken. Het was een mooi einde van dit concert. 

Het was leuk om weer een keer op zo’n open podium te spelen waarbij we nog meer mensen kennis hebben kunnen laten maken met de veelzijdigheid van accordeonmuziek! 
