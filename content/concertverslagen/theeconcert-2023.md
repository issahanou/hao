---
title: Theeconcert Heemstede
description: Het orkest was te gast in de Oude Kerk Heemstede in de serie.
image: /images/fotoboeken/2023-theeconcert/IMG-20230416-WA0002.jpg
author: Joke
alt: theeconcert
date: 16 april 2023
date-order: 2023-04-16
---

Op zondag 16 april waren wij te gast tijdens het theeconcert in de Wilhelminakerk
te Heemstede. Wij werden hartelijk ontvangen. Het publiek stroomde toe en dat is geweldig voor
een muzikant.

<img class="content-image content-image--right" src="/images/fotoboeken/2023-theeconcert/IMG-20230416-WA0002.jpg"/>

Omdat alles tot de puntjes was geregeld kon (de) *Intercity* (van Adolf Gotz) op tijd
vertrekken. Dit muziekstuk is speciaal voor accordeonorkest geschreven.

Ook het 2e muziekstuk is gecomponeerd speciaal voor accordeonorkest.
*Cubano*, door Wolfgang Russ-Plötz. 

De *Westside Story* behoeft geen uitleg, Leonard Bernstein en gearrangeerd door
Heinz Ehma Fugman. Dit is toch wel een ode aan aan klassieker
Het verhaal speelt zich af in het New York van de late jaren vijftig. De buurt is het
slachtoffer van de vernieuwing. De winkel van de bejaarde Valentina houdt dapper
stand. De wijk gaat gebukt onder de strijd tussen twee jeugdbendes. De Puerto Ricaanse
Sharks vechten voor een plek in de maatschappij en staan lijnrecht tegenover de Jets.
Oud-Jetslid Tony is net vrijgelaten uit de bak en wil een rustige nieuwe start maken. Dan
gebeurt het onvermijdelijke: tijdens een schoolfeest wordt hij stapelverliefd op de
Puerto Ricaanse Maria.

*Passacaglia & Fuga* van J.S. Bach en gearrangeerd door Leo van Lierop. Aan de
reacties uit het publiek te merken dat deze mooi gespeelde muziek gewaardeerd
werd. Het is een orgelstuk van Johann Sebastian Bach. Vermoedelijk gecomponeerd in het begin
van Bachs carrière. Het is een van zijn belangrijkste en bekendste werken. De variaties van de
passacaglia zijn zo ingenieus met elkaar verweven dat men zich altijd blijft verbazen.

*Adiós Nonino*. Iedereen herinnert zich nog wel die traan bij het huwelijk van toen
nog Prins Willem Alexander en Maxima nu ruim 21 jaar geleden. Carel
Kraayenhof speelde op de bandoneon. Dat dit stuk ook heel mooi kan worden
gespeeld door een accordeonorkest werd vandaag bewezen. Soliste Issa Hanou
speelde dit prachtige muziekstuk, gearrangeerd door An Raskin, die verbonden is
als hoofdvakdocente accordeon aan het Koninklijk conservatorium van Den
Haag. Issa studeert op 9 juni af aan het Koninklijk Conservatorium van Den Haag.

De *Notenkraker-suite* van Pjotr Tsjaikovski vertelt het verhaal van het meisje Clara. Op kerstavond ontvangt
zij een betoverende notenkrakerpop. In haar droom komt deze pop tot leven en
verandert in een prins. Clara en de prins worden al snel verliefd op elkaar.
Deel 1, ouverture en deel 3 de bloemenwals stonden vandaag op het
programma.

Als laatste speelden wij *Puszta*. Voor dit werk schreef Jan Van der Roost volksdansen in de geest van de
Slavische volksmuziek. Het karakter en de klank zijn vergelijkbaar met de
Hongaarse en Slavische dansen van Brahms en Dvorák, en met de Hongaarse
rapsodieën van Liszt. De melodieën zijn echter nieuw gecomponeerd en dus
geen arrangementen van bestaande dansen.

Na afloop werd nog even gezellig nagepraat over een zeer geslaagd concert met
thee en heerlijk zelf gemaakt gebak door de vrijwilligers.