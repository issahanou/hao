---
title: Pro Senectute Haarlem
description: We waren gevraagd om te spelen bij Pro Senectute Haarlem. 
image: static/images/nieuws/Pro_Senectute_Haarlem.JPG
author: Simone
date: 22 september 2024
date-order: 2024-09-22
---
Op 22 september 2024 waren wij uitgenodigd om te komen spelen bij Pro Senectute Haarlem. Dit is een woon-en zorglocatie voor senioren met een somatische zorgvraag. Het was heerlijk weer, dus we konden buiten spelen. Op zo’n mooie locatie is dat zeker geen straf. 

We speelden een gevarieerd programma dat goed aansloot bij het publiek. Zo speelden we twee tango’s: Tango Fugitativo & Milonga Feliz en Tango Sentimentale, drie potpourri’s: Edith Piaff, Westside Story en Brasilia en nog wat Spaanse klanken met Alcazar. Bij de potpourri’s werd er door het publiek mooi meegezongen en zo maakten we er met elkaar een gezellige middag van. 
