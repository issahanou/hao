---
title: Concert Schoterhof
description: Wij waren op bezoek bij het Schoterhof verzorgningstehuis.schot
image: /images/nieuws/Schoterhof.jpg
author: Joke
alt: schoterhof
date: 02 december 2023
date-order: 2023-12-02
---

Zaterdag 2 december was het zover. Een zeer special optreden in het Schoterhof.
Hilda, jarenlang trouw lid van ons orkest, woont daar nu en was zeer verheugd ons te mogen ontvangen.

Speciaal voor dit optreden hebben we gekozen voor een iets lichter maar zeker een heel mooi programma.  De grote ruimte liep al snel vol.

Het publiek werd verrast met bekende composities, o.a. de bloemenwals uit de notenkrakersuite, bekende melodieën uit de West Side Story, en natuurlijk ook de bekendste tango sinds 02-02-2002, Adiós Nonino, met de mooiste traan van, nu onze koningin, Maxima.

Ook speelden we een arrangement speciaal voor accordeonorkest. De Franse sfeer was meteen voelbaar en bezorgde mij kippenvel toen het publiek spontaan neuriede op de klanken van Edith Piaf. Wat is het toch mooi dat muziek dit teweeg brengt.

Al met al een fijne middag voor de bewoners  en ook de orkestleden hebben genoten van dit optreden. 

<img class="content-image content-image--full" src="/images/nieuws/Schoterhof.jpg">