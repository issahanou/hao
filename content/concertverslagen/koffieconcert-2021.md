---
title: Koffieconcert na de lockdown
description: Na minder te kunnen hebben gespeeld door de lockdown, heeft het orkest op 14 november 2021 weer een concert kunnen geven.
image: /images/agenda/koffieconcert-leo.jpg
author: Jolijn
alt: koffieconcert
date: 14 november 2021
date-order: 2021-11-14
---
Na ruim een jaar geen optreden gegeven te hebben, een nieuwe dirigent en
nieuwe corona maatregelen in het vooruitzicht was het zondag 14 november
echt weer even wennen om alle stoelen en standaards op te stellen, in te
spelen en een concert te geven voor publiek.

Aan de andere kant waardeer je alles des te meer nu we wisten dat het best
weer eens zou kunnen duren voordat het volgende concert zou komen!

De locatie van het optreden was nieuw voor ons: circus Hakim.

Wat een creatieve plek, de tribune: niet meer gloednieuwe stof op de stoelen,
voor de ingang van het toilet een piano en kruip door sluip door naar de ruimte
waar we onze instrumenten neer konden zetten. Deze creatieve plek konden
we wel gebruiken want de bassist was helaas op het laatste moment afwezig
en dus moest er geïmproviseerd worden.

Maaike op de bas, ik zou er niet aan moeten denken zo op het laatste moment
maar Maaike wilde het proberen en wat heeft ze het goed gedaan!

Om 10.45 uur begon het concert, we speelden:

- Werziade 3
- Holberg Suite (Prelude, Sarabande, Gavotte-Musette-Gavotte, Aria, Rigaudon)
- Spanischer Tanz
- Passacaglia &amp; Fuge
- ARTango
- Conga del Fuego Nuevo

Best een lang programma vond ik om achter elkaar door te spelen, niet zozeer 
voor het publiek wel voor mij als speler. Bij het laatste nummer wist ik niet
meer zo goed hoe ontspannen te zitten en levendig te spelen, de speeluren zijn
helaas toch wat verminderd in coronatijd en dat merk ik!

Veel positieve reacties vanuit het publiek, mooi programma, afwisselend en
goed afgewerkte stukken. Na afloop kreeg Leo een mooie baton als bedankje
voor zijn eerste concert als dirigent i.p.v. spelend lid. Een goede start voor nog
veel mooie concerten die zullen volgen!

<img class="content-image content-image--full" src="/images/nieuws/koffieconcert14-11-21.jpg"/>
