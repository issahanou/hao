---
title: Reis naar Innsbruck
description: In 2019 heeft het orkest wederom deelgenomen aan het World Music Festival in Innsbruck, waar wij in de Hochstufe uitkwamen. Naast een mooi concert was het ook een hele gezellige reis met het orkest.
image: /images/fotoboeken/2019-innsbruck/DSC_0753-small.JPG
author: Issa
alt: innsbruck
date: 30 mei tot en met 2 juni 2019
date-order: 2019-05-30
---
In 2019 zijn we afgereisd naar Innsbruck in Oostenrijk om deel te nemen aan het World Music Festival. Na een lange busreis op woensdag kwamen wij 's avonds aan in het hotel.
Wij zaten iets buiten de stad, mooi in de bergen gelegen. Met z'n allen hebben we nog even een drankje gedaan voor we ons bed opzochten.

<img class="content-image content-image--right" src="/images/fotoboeken/2019-innsbruck/DSC_0792-small.JPG"/>

De donderdag begon met een lekker ontbijtje en een repetitie. Daarna is een groot deel van het orkest met de lift omhoog gegaan waar ze bovenop de berg hebben rondgewandeld en lekker hebben gelunchd. Er lag zelfs sneeuw! Een kleiner groepje heeft de toeristische tram genomen naar de stad en zich daar in de middag vermaakt. 's Avonds ontmoette iedereen elkaar weer bij een pizzeria in het dorp om lekker te eten. Daarna is nog een korte repetitie gedaan en heeft ook het octet nog even kunnen spelen in het hotel.

Vrijdag was het dan zover. 's Ochtends is er nog kort ingespeeld in het hotel waarna alle instrumenten in de bus werden geladen en we naar de concertzaal vertrokken. Backstage werd in spanning afgewacht en nog even geluisterd naar de Ballet Suite die op dat moment werd uitgevoerd, een stuk waar wij zelf een deel van hebben gespeeld op ons tangoconcert in 2015.

<img class="content-image content-image--left" src="/images/fotoboeken/2019-innsbruck/DSC06882-small.JPG"/>

In een half uur moesten we opbouwen, soundchecken, spelen en weer afgaan, dus het was doorwerken. Na een dik jaar oefenenen waren wij ontzettend tevreden over het resultaat en hebben we mooi gespeeld. 

Na ons eigen optreden hebben we nog geluisterd naar Nurnberger orkest o.l.v. Stefan Hippe en het NSAO, waar onze eigen Leo en Ron nogmaals speelden. Daarna zijn we teruggegaan naar ons hotel om de instrumenten weer op te bergen en is een deel in het dorp gaan eten terwijl een ander groepje de stad weer introk om nog even na te genieten van ons optreden.

<img class="content-image content-image--right" src="/images/fotoboeken/2019-innsbruck/hotel.jpg"/>

Op zaterdag heeft iedereen zichzelf vermaakt, terwijl de ene nog een keer de berg ter voet beklom, zocht de ander een terrasje op en weer een ander zat de hele dag in de zaal om de andere orkesten. 's Avonds hebben wij met het hele orkest het avondconcert bezocht, wat opende met een accordeonist met strijkerskwartet die een paar indrukwekkende staande bellowshakes liet horen. De rest van de avond was er nog een divers programma en heeft iedereen nog even geluisterd naar het orkest van Hans-Gunther Kolz.

Zondag was het weer tijd om naar huis te gaan, dus zaten we al vroeg in de bus terug naar huis. Ons avontuur werd nog wat langer toen de bus iets na Frankfurt pech kreeg halverwegde de middag. Na een uur langs de snelweg wachten konden we gelukkig opgevangen worden in een dichtbijzijnd hotel, waar we ook wat konden eten. Uiteindelijk kwam de nieuwe bus uit Nederland aan, en konden we om 1 uur weer vertrekken naar Haarlem. 

Voor mij was het de eerste keer Innsbruck. Het was een ontzettend gezellige reis met veel muziek en mooie verhalen om thuis te kunnen vertellen!

Issa Hanou  