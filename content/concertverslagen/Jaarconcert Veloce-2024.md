---
title: Jaarconcert met Veloce 
description: Het jaarconcert was dit jaar in samenwerking met Veloce
author: Simone
date: 15 juni 2024
date-order: 2024-06-15
---

Op 15 juni 2024 was het tijd voor ons jaarconcert in de Wilheminakerk in Haarlem. Deze hadden we een mooie muzikale samenwerking met Veloce. Veloce is ook een accordeonorkest en staat onder leiding van Leo van Lierop, net als het Haarlems Accordeon Orkest. In Veloce spelen accordeonisten vanuit heel Nederland die het leuk vinden om samen op niveau muziek te maken. 

Het HAO begon het concert met de mooie Symphonic Overture van James Barnes. Daarna gingen we door met Fantasia Iberica van Gerhard Mohr. 

Veloce opende met Karnaval van Matthew Scott. Daarna gingen ze door met de Capriol Suite (Peter Warlock, arrangement Ralf Schwarzien) en Divertimento van Fritz Dobler. Na de pauze startten ze met het bekende Copacabana, geschreven door Jack Feldman & Barry Manilow, maar een arrangement van Tim Fletcher. Ze eindigden met Chaconne van Hans Josef Wedig. Een stuk waar ook veel leden van HAO herinneringen aan hebben. 

HAO sloot af met drie stukken. Om nog wat tango erin te gooien, speelden zij eerst Tango Fugitativo en Milong Feliz van Gerie Daanen. Daarna was het muziekstuk ‘Clowns’ van Ian Watson aan de beurt. Dit hebben wij ook in Londen gespeeld en Ian heeft het toen zelf gehoord. We sloten de avond af met Baile de Fantasia van Carl Wittrock. 

Het was een mooi, muzikaal programma. De goede samenwerking zorgde er ook voor dat het een mooi concert was. 
