---
title: Concert titel
location: Oude Kerk, Heemstede
date: 1959-09-01 01 00:00
admission: NTB
image: /images/agenda/zomipo.jpg
alt: zomipo
link: mailto:haarlemsaccordeonorkest@gmail.com
---

Kopieer deze template, hernoem de file, pas de velden hierboven aan en verwijder all text buiten de ---
Al deze velden moeten worden ingevuld:
- title: gewoon de titel
- location: waar is het concert, volledig adres
- date: datum in de volgende vorm: yyyy-mm-dd ww hh:mm, dus jaar, maand en dag, dan een spatie en dan de dag van de week (in twee cijfers), dus een concert op zaterdag krijg `06` en dan de tijd in uren en minuten
- admission: info over de toegangsprijs
- image: dit is de link naar de image die je wilt gebruiken. De link moet verwijzen naar de `static` folder, en dan beginnen met `/images/`
- alt: dit is de omschrijving van de foto
- link: dit kan bijvoorbeeld de `/kaartjes` pagina zijn of zoals die nu staat: de opent een mail venster naar de orkest mail