---
title: Uitwisseling NSAO
subtitle: Het tweede concert van onze uitwisseling zal plaatsvinden in Hilversum. 
location: Morgenster, Hilversum
date: 2023-06-10 06 20:00
admission: Kijk op nsao.nl
image: /images/agenda/nsao.jpg
alt: NSAO
link: 'https://nsao.nl'
---