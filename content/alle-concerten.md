## Optredens sinds 1985

### Onder leiding van Evert van Amsterdam

* 6 september 1998 Concert Haarlemmerhout
* 24 januari 1999 Koffieconcert Hildebrandzaal m.m.v. Accordeonorkest Air and Tune
* 10 april 1999 Jubileumconcert 40- jarig bestaan m.m.v. Dubbel Blaaskwintet en Kennemer Accordeongroep - Nova-college
* Kennemer Accordeongroep - Nova-college
* 18 september 1999 Lunchconcert in de St. Bavokerk
* 17 december 1999 Kerstconcert m.m.v. New Spirit - Lutherse Kerk
* 15 april 2000 Voorjaarsconcert m.m.v. Noordhollands Promenadeorkest en Air and Tune - Concertgebouw
* 7 april 2001 Voorjaarsconcert m.m.v. Forzando en KAG - Schoter Scholengemeenschap
* 23 maart 2002 Voorjaarsconcert m.m.v. Haarlems Clarinet Choir - Remonstrantsekerk
* 7 april 2002 Uitwisseling met Accordeonvereniging Forzando - Amsterdam
* 9/10 november 2002 Uitwisselingsconcert met Osnabrück - in Osnabrück
* 15 december 2002 Huize Agnes - Heemskerk
* 24 december 2002 Concert Velserduin - Santpoort
* 20 september 2003 Presentatie van de CD "Impressies" - Lutherse Kerk
* 29 november 2003 Concert met Haarlems Clarinet Choir - Kerk de kleine Vermaning
* 27 maart 2004 Jumelageconcert Osnabrücker Akkordeon Ork. - Doopsgezinde Kerk
* 6 maart 2005 Matinee concert - de Burghave Heemstede
* 5 juni 2005 Concert Haarlemmerhout
* 13 november 2005 Jaarconcert m.m.v. Spaarndams Gemengd Koor - Oosterkerk
* 11 december 2005 Orkestenconcours Goor
* 29 januari 2006 Concert Nieuw Delftwijde
* 12 maart 2006 Concert St. Jacob in de Hout
* 20 juni 2006 Concert Reinaldahuis
* 9 december 2006 Concert Philharmonie m.m.v. Zangroep To the Point en Air and Tune
* 14 april 2007 de Wildhoef - Bloemendaal
* 16 t/m 20 mei 2007 Concertreis Innsbruck, Internationale Orkesten Concours
* 22 september 2007 Concert in de St. Bavo - Haarlem
* 8 december 2007 Jaarconcert Lutherse Kerk m.m.v. Swingkoor Stardust
* 18 juni 2008 Concert op de Grote Markt tijdens de Haarlem Cultuurweek
* 24 augustus 2008 Koepelconcert Haarlemmerhout.
* 8 september 2008 Repetitieweekend Egmond
* 14 december 2008 Orkestenconcours NOVAM Hengelo
* 25 januari 2009 Jubileum concert Philharmonie
* 14 juni 2009 Concert tijdens MZK festival Philharmonie
* 20 september 2009 Concert Oude Kerk - Heemstede
* 3 oktober 2009 St. Bavokerk - Haarlem
* 11 oktober 2009 Concert met Accordeana - Helmond
* 17 januari 2010 Concert - Nieuw-Vennep
* 18 april 2010 Jaarconcert Oosterkerk - Haarlem
* 2 oktober 2010 St. Bavokerk - Haarlem
* 21 november 2010 Oosterkerk met Accordeana - Haarlem
* 16 april 2011 Jaarconcert Adelbertuskerk - Haarlem
* 5 november 2011 REL festival - Lelystad
* 14 december 2011 Bennebroekse Culturele Kring - Bennebroek
* 22 januari 2012 Zondag Middag Podium - Zwanenburg
* 25 maart 2012 Jaarconcert en CD presentatie Philharmonie - Haarlem
* 11 november 2012 concert samen met AE 2000 in de kerk van de Doopsgezinde gemeente in Hoofddorp
* 20 april 2013 concert Lutherse kerk te Haarlem
* 9 t/m 12 mei 2013 Wereld Musik Festival Innsbruck
* 14 september 2013 gastoptreden bij het concert van het NJAO Castricum
* 17 november 2013 Oude kerk Heemstede
* 21 april 2014 Zwanenburg Zie hier het programma
* 25 mei 2014 Accordeondag in Tilburg
* 22 november 2014 O.L.V onbevlekt ontvangen te Overveen met Duo D'Air en AE2000
* 15 februari 2015 tijdens dienst in Nicolaaskerk te Krommenie
* 15 maart 2015 concert in Warmond
* 26 juni 2015 bij eindexamen Accordeon van Leo van Lierop in Tilburg
* 28 juni 2015 in de muziekkoepel van de Haarlemmerhout
* 7 november 2015 in Velserduin
* 28 november 2015 Tangoconcert in het Schoter te Haarlem
* 21 februari 2016 op het Zondagmiddagpodium van Pier K. in Nieuw-Vennep
* 28 mei 2016 in de Philharmonie Haarlem tezamen met Accordeon-Orchester 1980 uit Oberhausen/ Dinslaken
* 12 november 2016 Meet&Play in Hoofddorp georganiseerd door AE2000
* 5 februari 2017 concert tijdens dienst in Nicolaaskerk te Krommenie
* 9 april 2017 Novam Orkesten Festival in het Akoesticum te Ede
* 10 juni 2017 jaarconcert in samenwerking met het koor Rumours in de Wilhelminakerk
* 9 september 2017 in Doopsgezinde kerk tijdens Haarlems Cultuurfestival
* 26 november 2017 optreden in Dinslaken tezamen met Accordeon-Orchester 1980 uit Oberhausen/ Dinslaken
* 8 april 2018 1e prijs Auditoriumzaal NOVAM orkestenfestival te Ede
* 9 september 2018 tijdens Oecumenische dienst Assendelft
* 11 november 2018 Zondagmiddagpodium Vijfhuizen
* 26 januari 2019 jubileumconcert Doopsgezinde kerk Haarlem
* 10 maart 2019 zondagmiddagpodium Velsen-Noord
* 30 mei - 2 juni 2019 World Music Festival Innsbruck
* 23 november 2019 2e jubileumconcert Doopsgezinde kerk Haarlem

### Onder leiding van Jorina Dekker

* 3-4 maart 1989 Clementi Festival - Uithoorn
* 14 maart 1989 Huize Agnes
* 21 maart 1989 Elizabeth Gasthuis 11 april 1989 Boerhavekliniek
* 29 mei 1990 Concert Dienstencentrum Noord m.m.v. Tesselschadekoor
* 30 april 1991 Koninginnedagconcert - Huis in de Duinen - Zandvoort
* 2 juni 1992 Concert Muziekpaviljoen
* 27 maart 1993 Voorjaarsconcert in de Linge m.m.v. zangkoor "Samen op weg"
* 23 oktober 1994 Concert Kennemer Gasthuis locatie Zeeweg
* 8 november 1997 Concert m.m.v. Shantykoor
    
### Onder leiding van Tom Wilmer

* 18 april 1985 Jaarconcert m.m.v. Spaarnebazuin
* 23 april 1985 Concert Molenburg
* 5 mei 1985 Muziektent Spaarndam
* 18 september 1985 Reinaldahuis
* 10 november 1985 Hoogwoud, deelname Festivalklasse
* 25 januari 1986 Concert Molenburg
* 10 april 1986 Jaarconcert de Linge m.m.v. Operettegezelschap N.O.G.
* 18 mei 1986 Concert Muziekpaviljoen
* 29 november 1987 Deelname NOVAM concours
* 29 juni 1988 Concert Openluchttheater Bloemendaal