---
title: Jubileum Live CD
description: Onze cd '60-jaar Haarlems Accordeonorkest' is uit. Deze live cd is opgenomen tijdens het jubileumconcert op 26 januari 2019. 
image: /images/nieuws/cd-60jaar2.png
alt: Live CD
date: 2019-11
link: /orkest/cds
linkText: Bekijk het CD overzicht
---