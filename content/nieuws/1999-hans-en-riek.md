---
title: Aankondiging 40-jarig jubileum
description: Hans en Riek van den Bogaart (zoon en moeder) kondigen in dit artikel het 40-jarig jubileum aan van Accordeonorkest Esmeralda. Ze vertellen over het instrument en hoe ze erbij zijn gekomen.
image: /images/nieuws/artikelen/1999-09-hans-en-riek-small.jpeg
alt: hans-en-riek-van-den-bogaart
date: 1999-04
link: /nieuws/oude-artikelen
linkText: Lees hier het artikel
---