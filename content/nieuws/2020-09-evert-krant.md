---
title: "'Zonder accordeon word ik ongelukkig'"
description: Naar aanleiding van het afscheidsconcert van dirigent Evert van Amsterdam, heeft het Haarlems Dagblad hem geïnterviewd.
image: /images/nieuws/2020-09-evert.jpeg
alt: Evert van Amsterdam
date: 2020-09
link: /nieuws/evert-afscheid
linkText: Lees hier het krantenartikel
---