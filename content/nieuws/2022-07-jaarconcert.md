---
title: Geslaagd jaarconcert!
description: Na twee jaar hebben wij eindelijk weer een concert kunnen geven en zelf een avondvullend programma weten neer te zetten. Met een intermezzo van het Duo Issa Hanou en Kaat Vanhaverbeke was het een ontzettend leuk concert!
image: /images/nieuws/groepsfoto.JPG
alt: Groepsfoto
date: 2022-07
link: /media/fotoboeken
linkText: Bekijk hier de foto's!
---