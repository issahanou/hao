---
title: We mogen weer!
description: Het afgelopen jaar hebben wij door de lockdown weinig samen kunnen spelen. Toch hebben wij elke vrijdag elkaar online gezien, webinars gevolgd en een escape room gedaan! Nu mogen we weer concerten geven!
image: /images/nieuws/Jaarconcert_jan-2019.jpg
alt: Nieuwe concerten
date: 2021-09
link: /agenda
linkText: Bekijk hier de agenda
---