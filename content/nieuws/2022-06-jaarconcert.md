---
title: Ons jaarconcert komt eraan!
description: Wij kijken er ontzettend naar uit om na twee jaar weer een concert te geven zoals we dat het liefst doen, met een volle zaal
image: /images/agenda/jaarconcert.jpg
alt: Jaarconcert
date: 2022-06
link: /kaartjes
linkText: Koop hier uw kaartjes!
---