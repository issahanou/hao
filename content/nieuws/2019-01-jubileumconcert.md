---
title: Jubileumconcert
description: In 2019 bestaat het Haarlems Accordeonorkest 60 jaar. In deze gelegeheid geven wij twee grote concerten dit jaar in de Doopsgezinde Kerk in Haarlem. 
image: /images/nieuws/Jaarconcert_jan-2019.jpg
alt: Jubileumconcert
date: 2019-01
link: /nieuws/60jaar-jubileum
linkText: Lees hier het krantenartikel
---