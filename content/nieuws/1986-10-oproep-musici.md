---
title: Oproep musici
description: In 1986 plaatste het orkest een advertentie om musici op te roepen mee te komen spelen in het orkest
image: /images/nieuws/artikelen/1986-10-oproep-musici-small.PNG
alt: krantenartikel
date: 1986-10
link: /nieuws/oude-artikelen
linkText: Lees hier het artikel
---