---
title: Terugblik op een muzikale uitwisseling
description: In oktober hebben wij het eerste deel van deze uitwisseling verzorgd, het was een prachtig concert met twee orkesten in de Oosterkerk in Haarlem. In juni zijn wij naar Hilversum gegaan voor de tweede helft. De kerk klonk prachtig en wij hebben ontzettend genoten van het spelen en luisteren!
image: /images/agenda/uitwisseling-2023.jpeg
alt: Uitwisseling
date: 2023-06
link: /media
linkText: Kijk hier de opnames terug
---