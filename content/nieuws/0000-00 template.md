---
title: Titel
description: Omschrijving
date: 2000-01
image: /images/nieuws/lao.jpg
alt: Londen
link: ""
linkText: ""
---


Kopieer deze template, hernoem de file, pas de velden hierboven aan en verwijder all text buiten de ---

Al deze velden moeten worden ingevuld:
- title: gewoon de titel
- description: dit is de tekst in het nieuwsitem, dit kan een paar zinnen zijn maar maak het niet te lang
- date: datum in de volgende vorm: yyyy-mm dus jaar en maand
- image: dit is de link naar de image die je wilt gebruiken. De link moet verwijzen naar de `static` folder, en dan beginnen met `/images/`
- alt: dit is de omschrijving van de foto
- link: Dit kan een externe link zijn, bijvoorbeeld `https://www.iets.nl` of een link naar de website, bijvoorbeeld `/agenda`
- linkText: Dit is de text in de knop van het nieuws item
Als je geen link wilt gebruiken, zet dan `link` en `linkText` op ""