---
title: Het seizoen zit er weer op!
description: Na een heet optreden in Hoofddorp zit seizoen 2022-2023 er weer op. Wij zijn al begonnen met de voorbereidingen voor het nieuwe seizoen, in november spelen wij op het Orkestenfesival van de NOVAM en in april 2024 gaan we naar Londen!
image: /images/agenda/uitwisseling-2016.jpg
alt: Uitwisseling
date: 2023-07
link: /concertverslagen
linkText: Lees hier het concertverslag
---