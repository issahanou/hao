---
title: Zin in een muzikaal voorjaar
description: Wij zijn druk bezig met de voorbereidingen voor dit voorjaar. Er staan al drie concerten op de planning met alle drie een heel verschillend karakter. Wij hebben er al heel veel zin in, zien we jullie op één (of meer) concerten terug?
image: /images/nieuws/boeken.jpg
alt: Orkest
date: 2023-02
link: /agenda
linkText: Zie hier de concert informatie
---