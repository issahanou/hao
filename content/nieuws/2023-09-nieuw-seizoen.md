---
title: Start van het nieuwe seizoen!
description: De zomer is weer voorbij en wij beginnen met de voorbereidingen van het nieuwe seizoen. In november spelen wij op het NOVAM orkestenfestival, in januari bij een Zondag Middagpodium, in april gaan we naar Londen en in juni hebben we een jaarconcert! 
image: /images/agenda/festival_hoofddorp2023.jpeg
alt: Nieuw seizoen  
date: 2023-09
link: /agenda
linkText: Bekijk de agenda
---