---
title: Jubileumjaar
description: 2019 was ons 60-jarig jubileumjaar! Daarin hebben wij in mei 2019 meegedaan aan het World Music Festival in Innsbrück. Zie de foto's in ons fotoboek en de video's van deze mooie reis!
image: /images/nieuws/innsbruck2019.JPG
alt: innsbruck reis
date: 2019-05
link: /media/fotoboeken
linkText: Bekijk hier de foto's
---