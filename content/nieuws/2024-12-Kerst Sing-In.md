---
title: Kerst Sing-In Adelbertuskerk
description: Op 15 december 2024 bieden wij de muzikale begeleiding bij de kerst Sing-In in de Adelbertuskerk Haarlem. 
date: 2024-12
image: /drukwerk/Poster-sing-along2024.jpeg
alt: Adelbertuskerk
link: /agenda
linkText: Bekijk hier de aankomende concerten
---
