---
title: Promotie Eredivisie
description: In 2005 promoveerde het orkest naar de eredivisie, na de eerste prijs te winnen bij het NOVAM conourse in de eerste divisie. 
image: /images/nieuws/artikelen/2005-12-promotie.jpeg
alt: krantenartikel
date: 2005-12
link: /nieuws/oude-artikelen
linkText: Lees hier het artikel
---