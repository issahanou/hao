---
title: Geslaagd koffieconcert
description: Na een ruim jaar geen concert te kunnen geven en een half jaar zonder repetities, konden we dan eindelijk weer! Wij hebben erg genoten van een prachtig concert in Circus Hakim.  
image: /images/nieuws/koffieconcert14-11-21.jpg
alt: Eerste concert
date: 2021-11
link: /media/concert-14-nov-21
linkText: Kijk hier de opnames van het concert terug
---