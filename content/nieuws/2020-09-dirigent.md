---
title: Nieuwe dirigent
description: In 2020 hebben wij afscheid genomen van onze dirigent Evert van Amsterdam na 22 jaar onder zijn leiding te hebben gespeeld. Vanaf medio september neemt Leo van Lierop het stokje over.
image: /images/nieuws/Leo.jpg
alt: Leo van Lierop
date: 2020-09
link: /orkest/dirigent
linkText: Lees hier meer over onze dirigent
---