---
title: Jaarconcert op 15 juni
description: Na een geweldige orkestreis naar Londen (foto's en filmpjes volgen snel!) maken wij ons klaar voor ons jaarconcert. Deze zal plaatsvinden op 15 juni 2024 in de Wilhelminakerk in Haarlem. Wij verwelkomen ook accordeonorkest Veloce o.l.v. onze eigen dirigent Leo. De kaartverkoop is gestart dus wees er snel bij!
date: 2024-04
image: /images/nieuws/londen2024.jpeg
alt: Londen
link: /kaartjes
linkText: Koop snel je kaartjes!
---
