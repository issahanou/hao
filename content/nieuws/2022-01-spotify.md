---
title: We zijn te vinden op Spotify!
description: De afgelopen jaren hebben wij een paar CD's opgenomen, maar tegenwoordig luisteren veel mensen hun muziek ook via Spotify. Daar gaan wij graag in mee, en daarom hebben wij nu onze CD's ook daar geüpload!
image: /drukwerk/double_jubileum.png
alt: Spotify
date: 2022-01
---