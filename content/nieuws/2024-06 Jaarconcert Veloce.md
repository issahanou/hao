---
title: Jaarconcert met Veloce 
description: In juni speelden wij ons jaarconcert in samenwerking met Veloce. Dit resulteerde in een mooi concert. 
date: 2024-06
link: /concertverslagen
linkText: Lees hier het concertverslag
---