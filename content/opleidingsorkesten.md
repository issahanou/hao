## Opleidingsorkesten

### Air and Tune + Kennemer Accordeon Groep (KAG)
Bij Muziekschool Hart zijn opleidingsorkesten actief, 
waarin (jonge) accordeonisten die pas enkele jaren accordeonervaring hebben in aanraking kunnen komen met samenspelen. 
Deze orkesten repeteren wekelijks op de Houtweg 18 te Haarlem. 
Voor meer informatie zie [www.accordeonschoolhaarlem.nl](http://accordeonschoolhaarlem.nl)