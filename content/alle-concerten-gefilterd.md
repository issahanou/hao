## Gedenkwaardige optredens sinds 1985

### Onder leiding van Evert van Amsterdam

* 6 september 1998 Concert Haarlemmerhout
* 10 april 1999 Jubileumconcert 40- jarig bestaan m.m.v. Dubbel Blaaskwintet en Kennemer Accordeongroep - Nova-college
* Kennemer Accordeongroep - Nova-college
* 15 april 2000 Voorjaarsconcert m.m.v. Noordhollands Promenadeorkest en Air and Tune - Concertgebouw
* 9/10 november 2002 Uitwisselingsconcert met Osnabrück - in Osnabrück
* 20 september 2003 Presentatie van de CD "Impressies" - Lutherse Kerk
* 27 maart 2004 Jumelageconcert Osnabrücker Akkordeon Ork. - Doopsgezinde Kerk
* 11 december 2005 Orkestenconcours Goor
* 9 december 2006 Concert Philharmonie m.m.v. Zangroep To the Point en Air and Tune
* 16 t/m 20 mei 2007 Concertreis Innsbruck, Internationale Orkesten Concours
* 18 juni 2008 Concert op de Grote Markt tijdens de Haarlem Cultuurweek
* 25 januari 2009 Jubileum concert Philharmonie
* 18 april 2010 Jaarconcert Oosterkerk - Haarlem
* 5 november 2011 REL festival - Lelystad
* 25 maart 2012 Jaarconcert en CD presentatie Philharmonie - Haarlem
* 9 t/m 12 mei 2013 Wereld Musik Festival Innsbruck
* 22 november 2014 O.L.V onbevlekt ontvangen te Overveen met Duo D'Air en AE2000
* 26 juni 2015 bij eindexamen Accordeon van Leo van Lierop in Tilburg
* 28 november 2015 Tangoconcert in het Schoter te Haarlem
* 28 mei 2016 in de Philharmonie Haarlem tezamen met Accordeon-Orchester 1980 uit Oberhausen/ Dinslaken
* 9 april 2017 Novam Orkesten Festival in het Akoesticum te Ede
* 8 april 2018 1e prijs Auditoriumzaal NOVAM orkestenfestival te Ede
* 30 mei - 2 juni 2019 World Music Festival Innsbruck

### Onder leiding van Jorina Dekker

* 3-4 maart 1989 Clementi Festival - Uithoorn
* 14 maart 1989 Huize Agnes
* 21 maart 1989 Elizabeth Gasthuis 11 april 1989 Boerhavekliniek
* 29 mei 1990 Concert Dienstencentrum Noord m.m.v. Tesselschadekoor
* 30 april 1991 Koninginnedagconcert - Huis in de Duinen - Zandvoort
* 2 juni 1992 Concert Muziekpaviljoen
* 27 maart 1993 Voorjaarsconcert in de Linge m.m.v. zangkoor "Samen op weg"
* 23 oktober 1994 Concert Kennemer Gasthuis locatie Zeeweg
* 8 november 1997 Concert m.m.v. Shantykoor
    
### Onder leiding van Tom Wilmer

* 18 april 1985 Jaarconcert m.m.v. Spaarnebazuin
* 23 april 1985 Concert Molenburg
* 5 mei 1985 Muziektent Spaarndam
* 18 september 1985 Reinaldahuis
* 10 november 1985 Hoogwoud, deelname Festivalklasse
* 25 januari 1986 Concert Molenburg
* 10 april 1986 Jaarconcert de Linge m.m.v. Operettegezelschap N.O.G.
* 18 mei 1986 Concert Muziekpaviljoen
* 29 november 1987 Deelname NOVAM concours
* 29 juni 1988 Concert Openluchttheater Bloemendaal