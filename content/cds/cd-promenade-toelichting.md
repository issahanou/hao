# Haarlems Accordeon Orkest Esmeralda o.l.v. Evert van Amsterdam
Op 15 januari 2012 bestond het Haarlems Accordeonorkest Esmeralda 53 jaar. Het orkest heeft 28 leden en is  één van de meest vooraanstaande accordeonorkesten van Nederland.
In november 2011 won het orkest tijens het REL-Festival in Lelystad de publieksprijs. 
Het huidige repertoire van het orkest bevat een grote diversiteit aan muziekstijlen, naast werken in het klassieke genre worden ook composities gespeeld uit de volks-, swing-, en filmmuziek.

De CD *Promenade*, met alleen orginele werken voor accordeonorkest, is een CD op zeer hoog muzikaal niveau. 
Drie van de vijf gespeelde werken op de CD zijn voor het accordeon van historisch belang, zij komen uit de beginperiode van de orginele muziekliteratuur voor accordeon.
De drie werken zijn: Chaconnen (1956) van H.J. Wedig, Die Zertrümmerte Kathedrale (1963) van V. Trojan en La Campanella (1951) van R. Würthner. 
Daarnaast staan er twee meer recente werken op de CD, te weten Nostalgia (1982) van A. Schurbin, arr. H. Quackernack en Boardwalk (1965) van A. Wammes. 
Het orkest wil met het presenteren van de CD *Promenade* meer bekendheid geven aan alle facetten van het hedendaagse klassieke accordeon.

Het orkest kende naast het REL-Festival vele andere muzikale hoogtepunten, zoals de presentatie van de eerste CD in 2003,
het Nationaal Orkestenconours in 2005 te Goor (waar zowel promotie naar de eriedivisie als de NOVAM wisselbeker behaald werden) 
en deelname aan het Internationaal Muziekfestival te Innsbruck in 2007. 
In 2008 werd succesvol opgetreden tijdens het Muziekfestival op de Grote Markt in Haarlem. 
Op 25 januari 2009 vond het 50-jarig jubileum van het orkest plaats in een uitverkochte zaal van de Philharmonie te Haarlem, hiervan werd een live-cd gemaakt.

Het orkest staat sinds 1998 onder leiding van Evert van Amsterdam. Hij studeerde hoofdvak accordeon bij Miny Dekkers. 
Tijdens en na zijn studie speelde hij in diverse kamermuziekensembles o.a. D'accord, Ensemble D'Acht, Roemeens Balkanorkest Mihai Scarlat en Cuarteto Renuevo.
Evert van Amsterdam is sinds 1984 als docent verbonden aan de Muziekschool Zuid-Kennemerland te Haarlem, waar hij tevens dirigent is van de Kennemer Accordeongroep en Air en Tune.

## Toelichting bij de muziekstukken op de cd Promenade uit 2012

### Chaconne (1956) - H.J. Wedig (1898 - 1978)

Chaconne is ontstaan uit een oude Barokke dansvorm met een basmelodie die rustig en herhalend is (basso ostinato). Aan de melodie worden steeds nieuwe variaties toegevoegd.
De Compositie bestaat uit vier delen die in elkaar overgaan. Deel één is een korte inleiding met een eerste variatie, deze gaat over in een rustige wals van deel twee. Deel drie is een uitdrukkingsvolle Pastorale in 6/8 maat.
De compositie besluit met een temperatmentvol deel vier in een snelle 4-tels maat met veel contrasten. 

### Nostalgia (1982) - A. Schurbin (1945), arr.  H. Quakernack
Deze compositie bestaat uit de volgende zeven delen:

1. Ben ritmico
2. Ragtime intermezzo
3. Silencio
4. Valse tristesse
5. Tango
6. Ben ritmico
7. Nostalgia

De Russische componist Alexander Schurbin heeft onder andere gestudeerd bij A. Chatschaturjan en D. Shostakovich. 
Hij heeft samen met de fameuze bajan-accordeonist Friedrich Lips gewerkt aan deze compositie, die eignelijk als titel Sonate nr. 2 droeg en als subtitel Nostalgia, een werk met verschillende stijlen.
De subtitel is zo gekozen omdat in de compositie zeer bekende, ouderwetse ritmes en stijlen vorkomen. 
Na de première in 1980 kreeg de compositie veel waardering, omdat moderne instrumentale technieken worden gecombineerd met oude muzikale stijlen, zoals tango, wals en ragtime.

### Boardwalk (1995) - A. Wammes (1953)
Deze compositie bestaat uit drie delen:

1. Merry-go-round (carrousel)
2. Ferris wheel (reuzenrad)
3. Loopig (achtbaan)

De namen van de delen zijn een treffende bewoording van hoe de muziek zich laat ervaren. 
De componist combineert in de snelle delen (1 en 3) minimal music met swingende pop-bassen, later vermengd met krachtige virtuoze thema's.
Het tweede deel staat in groot constrast met het eerste en derde deel: het is polyfoon van opzet en heeft een meditatief en impressionistisch karakter.

### Die Zertrümmerte Kathedrale (1963) - V. Trojan (1907 - 1983)
Deze compositie van de Tsjechische componist Trojan gaat over de geschiedenis van de bekende kathedraal van Dresden: de Frauenkirche.
Het begint met een lieflijke beschrijving van de schoonheid van de kathedraal, waarna in de muziek het bombardement losbarst.
In dit bombardement dat plaatsvond in de nacht van 13 februari 1945 werd de 18de eeuwse kathedraal door Britse en Amerikaanse vliegtuigen vernield.
Van de kerk bleef enkel een door twee muren geflankeerde berg puin over. Deze ruïne werd het symbool van de verschikkingen van de Tweede Wereldoorlog. 
Na de val van het communisme en de Duitse hereniging werd de Frauenkirche vanaf 1994 weer opgebouwd. In 2005 is de kathedraal heropend.   

### La Campanella (1951) - R. Würthner (1920 - 1974)
*Solist: Vincent van Amsterdam*

De componist Rudolf Würthner heeft in zijn toonaangevende composities en arrangementen het accordeon laten zien als een veelzijdig concertinstrument.
Würthner was een groot componist, acccordeonist en docent, en richtte in 1947 het legendarische symfonische accordeonorkest van de firma Hohner op.
La Campanella uit 1951 is een concert-etude, gebaseerd op een thema van de componist en violist N. Paganini.
Würthner schreef de compositie voor accordeon-soilo en arrangeerde deze zelf tot de hier uitgevoerde versie voor solo-accordeon met orkest.

De solist in La Campanella is Vincent van Amsterdam, hij is 1e prijs winnaar van o.a. het Prinses Christina Concours (2007) en het Nationaal Accordeon concours (2008, 2009).
Op de Coupe Mondiale (2010) behaalde hij een 4e plaats (solo) en een 3e prijs in de kamermuziekklasse. Zie ook: [www.vincentvanamsterdam.nl](www.vincentvanamsterdam.nl).

## Orkestbezetting
<b>1e stem</b>:
Vincent van Amsterdam, Annemiek Knijnenburg, Leo van Lierop, Maaike Lips, Ron Paeper, Roelof Ruis.

<b>2e stem</b>:
Jolijn Bakker, Joëlle Dek, Emmy van Houten, Tanja van Opzeeland, Joke van Straten.

<b>3e stem</b>:
Else Nicolai, Rikie Paeper, Frieda Philippo, Jet van der Spek, Nienke Stange, Carla Vermeren, Emma Vermeulen, Priy Werry.

<b>4e stem</b>:
Hilda van Andel, Margreet de Haas, Petra van der Meij-van Bremen, Floor de Mulder, Christine Paeper-van Bremen, Rianne Prins.

<b>Piano</b>:
Stefan Rasch.

<b>Basaccordeon</b>:
Gilles Schuringa.

<b>Slagwerk</b>:
Jan van der Laan, Priy Werry.

<b>Dirigent</b>:
Evert van Amsterdam
