# Toelichting bij de muziekstukken op de cd Impressies uit 2003

Accordeonorkest Esmeralde uit Haarlem werd in januari 1959 opgericht, bijna 45 jaar oud presenteert het orkest nu haar eerste CD. 
Het repertoire van het orkest bevat een grote diversiteit aan muziekstijlen.
Door een brede programmering van klassiek tot pop blijven veel (ook jonge) accrodeonisten gemotiveerd voor het accordeonspel dat zich de afgelopen eeuw zo sterk ontwikkeld heeft.

De CD opent met de klassieke ouverture <span class="song-title">"Uber zwei Finnische Themen"</span> van George de Godzindky, het arrangement is van de grote componist, solist en dirigent Rudolf Würthner.
Een van de rustputen op de CD is <span class="song-title">"Air"</span> uit Synfonia all Barocco, dit betreft een werkje in barokstijl. 
Het orkest vervolgt met spanningsvolle filmmuziek van Jacob de Haan, de componist heeft de compositie opgedragen aan de beroemde schrijver van filmmuziek, de Italiaan Ennio Morricone.

<span class="song-title">"The Lord of the dance"</span> (Ronan Padraig Hardiman), een compositie met moderne en traditionele volksmuziek en <span class="song-title">"Slavonska Rhapsodija"</span> (A. Götz), een werk geënt op de Slavische volksmuziek, zijn werken die gecomponeerd zijn vanuit de oorsprong van het instrument.
<span class="song-title">"Praludium und Fuga in a-moll"</span> van Matyas Seiber is een compositie in de stijl van Buxtehude (leraar van J.S. Bach), beiden grote componisten uit de barokperiode.

Op de CD speelt het orkest drie tango's, ten eerste de Tango Nuevo <span class="song-title">"Adios Nonino"</span> van Astor Piazolla en twee delen uit <span class="song-title">"Tres composiciones pora orquestra de accordeon"</span> van Gerie Daannen, te weten Milonga en Tango guinar ojo. Deze drie composities betreffen moderne tango's, de muziek enerzijds introvert, sentimenteel en melancholiek, anderzijds opzzwepend en vurig. 
Chromatiek, klassiek contrapunt en een puntige ritmiek zijn elemenen die we steeds kunnen terugvinden in deze mzuiek. 

Accordeonorkest "Esmeralda" sluit de CD af met een arrangement van popklassieker uit de 70-er jaren: <span class="song-title">Music van John Miles</span>.

## Bezetting Accordeonorkest Esmeralda
<b>1e stem</b>:
Ron Paeper, Susanne Koeman, Christine Paeper-van Bremen, Jan Reijnders, Tanja van Opzeeland, Vincent van Amsterdam

<b>2e stem</b>:
Joke van Straaten, Jolanda Sanberg, Jolijn Bakker, Peter Honders, Gerard Hunting.

<b>3e stem</b>:
Hans van den Bogaard, Nienke Stange, Hilda van Andel, Frieda Philippo, Rikie Paeper.

<b>4e stem</b>:
Carla Vermeren, Gitta Koot, Petra van der Meij-van Bremen.

<b>Piano</b>:
Stefan Rasch.

<b>Basaccordeon</b>:
Niels Bruin.

<b>Slagwerk</b>:
Cor Mantel.

<b>Dirigent</b>:
Evert van Amsterdam

## De Dirigent
Evert van Amsterdam, vaste dirigent van "Esmeralda", studeerde hoofdvak accordeon aan het Utrechts Conservatorium bij Miny Dekkers,
hij speelde in diverse kamermuziek ensembles o.a. ensemble d'Accord, Trio Strano en ensemble D'Acht, thans musiceert hij in "Cuarteto Renuevo". 
Hij is als docent accordeon werkzaam op het Muziekcentrum Zuid-Kennemerland, waar hij ook twee accordeonorkesten dirigeert.