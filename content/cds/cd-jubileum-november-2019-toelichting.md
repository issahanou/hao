# Tweede jubileumconcert van het Haarlems Accordeonorkest op 23 november 2019

Het Haarlems Accordeonorkest sluit met deze live CD opname haar 60-jarig jubileumjaar af.
Samen met het Accordeonduo Maaike Lips & Leo van Lierop werd een spetterend concert gegeven in de Doopsgezinde Kerk.

HAO is opgericht in 1959 en heeft 26 leden, het behoort tot de top van de accordeonorkesten van Nederland. 
Het orkest speelt op deze CD de delen 1 t/m 6 en 10 van de Schilderijententoonstelling van M. Moessorgski.
De arrangeur T. Bauer heeft voor zijn bewerking voor accordeonorkest gekozen voor de versie voor symfonieorkest van M. Ravel, de meeste kleurrijke en populaire versie.
Op deze CD staat verder de bijzondere driedelige compositie "Opale Concerto". De kwalitatief hoogstaande accordeonsolo wordt gespeeld door Leo van Lierop.
Deel 1 en 2 zijn geschreven in zijn "New Musette" stijl, deel 3 heeft de "Tango Nuevo" kenmerken. 
Twee favorieten uit het repertoire van de afgelopen jaren mochten niet ontbreken op de CD: Boardwalk van A. Wammes en Slavonska Rhapsodija van A. Götz, met een virtuoze solo van Ron Paeper.

Het Accordeonduo verzorgt een deel van het programma met muziek van de componisten Reicha, Saint-Saëns en Widor. 
Het kwintet van Reicha laat invloeden uit de Franse klassieke muziek horen. 
Saint-Saëns en Widor waren beide organist, de gespeelde stukken zijn oorspronkleijjk geschreven voor orgel.
Saint-Saëns maakt in de 7e Improvisation gebruik van onverwachte modulaties en wendingen. 
De Toccata Symfonie van Widor heeft een glorieus en weelderig karakter.  


## Toelichting bij de muziekstukken op de jubileum-cd 23 november 2019

### Schilderijententoonstelling Modest Moessorgski (arr. Thomas Bauer)

Moessorgski schreef de zestiendelige schilderijententoonstelling nadat hij een expositie had bezocht van de schilder Viktor Hartman. Hartman, een vriend van Moessorgski, was kort daarvoor overleden.
Van Moessorgski's originele pianoversie zijn talloze bewerkingen gemaakt, de versie voor symfonieorkest van M. Ravel is de meest kleurrijke en populaire. De arrangeur voor accordeonorkest, Thomas Bauer, heeft deze versie als uitgangspunt genomen voor zijn arrangement. Het HAO speelt de volgende 11 delen:

* Promenade 1
* Gnomes
* Promenade 2
* Oude kasteel
* Promenade 3
* Tuilerieën
* Bydlo
* Promenade 4
* Kuikenballet
* Samuel Goldenberg en Schmuyle
* Grote poort Kiev

Als het orkest begint te spelen openen de deuren van de expositiezaal, de promenade die steeds in het stuk voorkomt symboliseert de wandelingen door de tentoonstelling van het ene naar het andere kunstwerk.
Na de eerste promenade staan we voor het eerste kunstwerk: de Gnoom, gebaseerd op een schilderij van een notenkraker in de vorm van een gnoom zonder lichaam, met handvaten als benen en een noten krakende kop. Een gnoom is een lelijke, kreupele dwerg, de muziek is luguber, macaber en heeft een slingerend ritme, wat de onhandige looppas van de Gnoom symboliseert.
Promenade 2: de promenades geven eenheid aan het stuk en zijn gebaseerd op een Russisch thema. We komen bij het schilderij Vecchio Castello: het oude kasteel. Op een middeleeuwse bourdon basbegeleiding, wat een draailier symboliseert, zingt een minstreel zijn lied.
Na de volgende promenade lopen we door naar de Tuilerieën, de Parijse tuinen waar we kinderen horen spelen. De dalende terts die de componist als basis gebruikt voor dit deel is een echt 'kinderinterval' ('klikspaan'). Eerst hoort men de jongens en daarna de meisjes. Het ritme suggereert hun voortdurende gekwebbel en spel.
Schilderij Bydlo hangt naast Tuilerieën, want er is geen promenade. Dit schilderij beeldt een voorttrekkende ossenkar uit, stap voor stap en zwaar beladen verdwijnt hij weer in de verte. Een sterke puls is hoorbaar in de muziek.
Hierna komt er weer een (korte) promenade.

Het contrast met het volgende schilderij, het vertederende ballet van de kuikens in hun eierschalen, is zeer groot. Onhandig wurmen de hupsende kuikentjes zich uit hun schaal.
Na het kuikenballet volgt Samuel Goldenberg en Schmuÿle, dit schilderij beeldt een ruzie uit tussen een rijke en arme Poolse jood. De muziek maakt gebruik van Joodse en Poolse thema's.
Met de grote poort van Kiev sluiten we dit werk groots af. De poort die Hartman tekende is volgens architecten zo imposant dat die zou bezwijken onder zijn eigen gewicht. Moessorgski verbeeld bij de grote Poort van Kiev een heilige processie met klaterende cimbalen en veel klokgelui. Het promenade thema wordt gecombineerd met 2 Russische thema's waarmee de schilderijententoonstelling groots en indrukwekkend afsluit.

### Stukken uitgevoerd door duo Maaike Lips en Leo van Lierop

#### Kwintet in e-mineur; Op. 88 No. 1-Allegro ma non Troppo Anton Reicha (arr. Maaike Lips)

De Boheemse componist Anton Reicha (1770-1836) volgde lessen compositie bij Johann Albrechtsberger en Antonio Salieri. Ook heeft hij contact gehad met Joseph Haydn. Reicha's leven wordt gekenmerkt door verschillende periodes en vele reizen. Zo heeft hij een tijd in Wenen gewoond, maar werd hij aan het eind van zijn leven Frans staatsburger. Een groot deel van zijn oeuvre omspant diverse kamermuziekwerken, waaronder een serie kwintetten voor blazers (fluit, hobo, klarinet, hoorn en fagot). In de tijd dat Reicha deze kwintetten schreef, had hij inmiddels een betrekking gekregen aan het conservatorium van Parijs. Derhalve zijn invloeden uit de Franse klassieke muziek in dit stuk te horen.

#### Improvisation, Op. 150 Nr. 7	Camille Saint-Saëns (arr. Tim Fletcher)

Camille Saint-Saëns (1835-1921) is vooral bekend geworden als componist, maar was in zijn tijd ook een zeer verdienstelijk organist. Ook was hij een belangrijk ambassadeur van de introductie van buitenlandse muziek in Frankrijk en van een heropleving van de Franse Barokmuziek. Zijn '7 Improvisations' zijn een bundel die deel uitmaken van zijn bescheiden reeks orgelstukken. In deze improvisaties maakt hij vaak gebruik van onverwachte modulaties en wendingen, maar ook van herkenbare thematiek.

#### Toccata Symfonie Nr. 5 Charles-Marie Widor (arr. Leo van Lierop)

Het laatste werk (Op. 42 No.1) is eveneens een bewerking van een orgelstuk; mogelijk een van de bekendste orgelwerken uit de muziekgeschiedenis. Charles-Marie Widor (1844-1934) voltooide zijn muziekstudie in Brussel en werd een van de meest gelauwerde organisten uit zijn tijd. Hij werd op proef aangesteld als organist bij de Saint-Sulpice in Parijs, een 'proeftijd' die uiteindelijk 64 jaar zou duren. Met zijn orgelsymfonieën dreef hij zowel instrument als speler tot uitersten. Zijn stukken hebben dan ook vaak een glorieus, weelderig karakter.

### Opale Concerto	Richard Galliano

*Solist: Leo van Lierop*

Richard Galliano studeerde vanaf 4-jarige leeftijd piano en accordeon bij zijn vader Lucien Galliano. Later studeerde hij op het conservatorium in Nice en volgde tevens de vakken harmonie, contrapunt en trombone.
Een belangrijk ontmoeting in het leven van Galliano vond plaats in 1980 met de Argentijnse componist en bandoneonist Astor Piazzolla. Astor moedigde hem sterk aan om de Franse 'New Musette' te gaan maken, zoals hij dat eerder had gedaan met zijn Argentijnse 'Nieuwe Tango'. Galliano is er uiteindelijk in geslaagd om verschillende muziekstijlen te mixen tot één persoonlijk klankidioom, dichtbij 'de jazz'. Galliano heeft met zijn muziek de accordeon en bandoneon meer meegenomen naar het concertpodium waardoor het instrument op nog meer podia een plek heeft gekregen. Tijdens zijn lange en productieve carrière heeft Richard Galliano meer dan 50 albums opgenomen. Hij werkt(e) samen met een indrukwekkend aantal prestigieuze artiesten en muzikanten als: Chet Baker, Gary Burton, Toots Thielemans, Kurt Elling, Juliette Greco, Charles Aznavour en Nigel Kennedy. Op deze cd staat een bijzondere 3-delige compositie Opale Concerto (1998) voor accordeonsolo en strijkorkest nu in een arrangement voor accordeonorkest. De accordeonsolo wordt gespeeld door Leo van Lierop. Deel 1 en 2 zijn geschreven in zijn 'New Musette' stijl, deel 3 heeft de 'Tango Nuevo' kenmerken. Een gedeelte van deel 3 is ook zelfstandig uitgebracht onder de naam 'New York tango'.

### Boardwalk Ad Wammes

Dit werk is opgenomen op de CD Promenade (2012) van HAO en bestaat uit 3 delen. De componist combineert in de snelle delen (deel 1 en 3) minimal music elementen aan een swingende pop-bas, later word dit vermengd met virtuoze volksmuziek elementen. Het meditatieve tweede deel gaat na een polyfoon begin over in een impressionistisch klankveld. Ad Wammes studeerde compositie bij Ton de Leeuw, Theo Loevendie en Klaas de Vries, piano bij Edith Lateiner-Grosz en electronische muziek bij Ton Bruynèl. Na een periode waarin hij hoofdzakelijk muziek schreef voor TeleacNOT, richt hij zich sinds 2005 helemaal op de wereld van de klassieke muziek. Het in 2007 door Boosey&Hawkes opnieuw uitgebrachte 'Miroir' betekende zijn internationale doorbraak. Dit werk wordt over de hele wereld gespeeld door vele toporganisten als Thomas Trotter en John Scott en verscheen reeds 17x op CD. Pianomuziek verscheen bij Schott en De Haske. Diverse orgelwerken zijn gepubliceerd bij Boosy&Hawkes. In 2018 componeerde Ad Wammes speciaal voor het Haarlems Accordeonorkest, het werk Sometimes voor het 60-jarig bestaan van het orkest.

### Slavonska Rhapsodija A. Götz

Adolf Götz studeerde op het conservatorium piano, viool, compositie en dirigeren en is een componist die zeer veel werken heeft geschreven voor accordeonorkest. Daarnaast heeft hij ook veel solowerken en pedagogische literatuur voor accordeon gecomponeerd. Götz is een geliefd componist bij het publiek, hij componeerde aantrekkelijke muziek, met veel moderne compositietechnieken. Een aantal van zijn werken waren folkloristisch van aard met daarin vaak spannende ritmische elementen. De componist heeft zich bij het schrijven van Slavonska Rhapsodija laten inspireren door de Slavische muziek. De compositie kenmerkt zich door veel verschillen in tempi en maatsoorten, waarbij meeslepende en opzwepende melodieën elkaar voortdurend afwisselen. De accordeonsolo wordt gespeeld door Ron Paeper.


## Orkest bezetting 2019
<b>1e stem</b>:
Leo van Lierop, Ron Paeper, Maaike Lips, Joëlle Dek, Emma Vermeulen, Tim Wagelaar.

<b>2e stem</b>:
Jolijn Bakker, Floor de Mulder, Simone Visser, Tanja van Opzeeland, Joke van Straten.

<b>3e stem</b>:
Priy Werry, Nina Reehorst, Thijs Paeper, Carla Vermeren, Rikie Paeper, Simone Vogel, Reinout Fonk.

<b>4e stem</b>:
Issa Hanou, Marian Belderok, Pelle Bruijs, Hans van Roode, Margreet de Haas, Petra van Bremen.

<b>Basaccordeon</b>:
Gilles Schuringa.

<b>Slagwerk</b>:
Willem ten Sythoff.

<b>Dirigent</b>:
Evert van Amsterdam

## De Dirigent
Evert van Amsterdam studeerde hoofdvak Accrodeon aan het Utrechts Conservatorium bij Miny Dekkers, met het bijvak Directie / Ensembleleiding.
Tijdens en na zijn studie speelde hij in diverse kamermuziekensembles, momenteel speelt hij in het tango kwartet Cuarteto Renuevo.

Hij heeft een grote accordeonschool opgebouwd in Haarlem en veel van de huidige spelers van het HAO opgeleid. 
Hij is niet alleen dirigent van het Haarlems Accordeonorkest maar ook van de Kennemer Accordeongroep en het jeugdorkest Air en Tune. 
Met alle orkesten heeft hij door de jaren heen prijzen gewonnen en cd's gemaakt.
Hij is een van de organisatoren van het jaarlijks Nationaal Accordeon Concours en voorzitter van de NOVAM.


## Accordeon Maaike Lips & Leo van Lierop
<b>Maaike Lips</b> (1986) speelt sinds haar elfde accordeon. Na lessen in Haarlem bij Evert van Amsterdam heeft zij hoofdvak klassiek accordeon gestudeerd bij Ronald van Overbruggen en Gerie Daanen aan het Fontys Conservatorium in Tilburg.
In juni 2012 behaalde zij haar bachelordiploma. Als solist won ze in 2011 en 2012 de 2e prijs in de hoogste afdeling bij het Nationaal Accordeon Concours en in 2013 met Duo D'Air de 1e prijs bij dit concours.
Maaike speelt in diverse ensembles, waaronder accordeonduo D'Air met Leo van Lierop, met Chantal van den Dungen (fluit) en het Haarlems Accordeonorkest.\
Als privé docent en aan de muziekschool in Breda ontwikkelde zij zich als accordeondocent voor leerlingen van alle niveau's.

<b>Leo van Lierop</b> (1991) studeerde Accordeon bij Ronald van Overbruggen en Gerie Daanen en HaFa-Directie bij Hardy Mertens aan het Fontys Conservatorium in Tilburg.
In 2015 rondde hij beide opleidingen cum laude af. 
Hij was diverse malen prijswinnaar bij het Nationaal Accordeon Concours en soleerde met diverse harmonie-, symfonie- en accordeonorkesten.
Leo vormt een duo met fluitiste Arjanne Bruggeman, speelt in Sexteto Recuerdo en in het Brecht Trio. 
Hij is dirigent van o.a. Accordeonvereniging Forzando (Amsterdam), Kunst na Arbeid Epe en het door hemzelf opgericht accordeonorkest Veloce uit Zoetermeer.
Met het laatstgenoemde orkest won hij in 2018 de programmaprijs op het HOVAM Orkestenfestival in Ede. 
   