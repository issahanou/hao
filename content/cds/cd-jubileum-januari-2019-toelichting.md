# Jubileumconcert van het Haarlems Accordeonorkest op 26 januari 2019

Het Haarlems Accordeonorkest viert in 2019 haar 60-jarig bestaan met een jubileumconcert in de Doopsgezinde Kerk te Haarlem. Deze CD bevat de live-opname van dit concert.
Het orkest is in 1959 opgericht onder de naam Esmeralda en heet sinds 2014 Haarlems Accordeonorkest. 
Het staat onder leiding van Evert van Amsterdam, heeft 26 leden en behoort tot de top van de accordeonorkest van Nederland.
Het orkest speelt een gevarieerd repertoire met symfoinsich-klassieke werken en composities uit de volks-, tango-, en pop-muziek.

Aan het jubileumconcert werken solisten Jeanine en Vincent van Amsterdam mee in repecteievelijk het vioolconcert Zigeunerweisen van Pable de Sarasate en het accordeonconcert La Campanella van Rudolf Würthner.
Het Van Amsterdam Duo verzorgt daarnaast ook een deel van het programma. 

Speciaal voor het orkest heeft de Nederlandse componist Ad Wammes een nieuwe compositie geschreven, die bij het jubileumconcert in première gaat. De drie delen geven weer wat zich in de geschiedenis van een orkest door de jaren heen kan afspelen:
1. Sometimes, times were good,
2. Sometimes, times were sad,
3. But most of the time, times were great.

De twee snelle delen hebben een stevige baslijen in pop/rock and rollstijl. Het middelste deel is rustig en meditatief. 
Een eerdere compositie van Ad Wammes, Boardwalk, is het Haarlems Accordeonorkest opgenomen op haar tweede CD *Promenade*. 


## Toelichting bij de muziekstukken op de jubileum-cd 26 januari 2019

### Symfonie nr. 9 – deel 4  - Antonín Dvořák (arr. T. Dalhof)
Het HAO opent het concert met het vierde deel uit Symfonie nr. 9, Uit de
Nieuwe Wereld. Dvořák schreef deze symfonie toen hij in Amerika
verbleef, aan het einde van de negentiende eeuw. Al zijn reiservaringen en
emoties zijn verwoord in deze bekende symfonie. Dvořák combineert
graag verschillende ritmes. Het is een populair, dramatisch en lyrisch werk
met een groot scala aan orkestrale kleuren.

### Rondo Capriccioso -  Wladislaw Solotarjow (arr. H. Quakernack)
Wladislaw Solotarjow (1942-1975) is een Russische componist die veel
originele werken voor accordeon/bayan heeft geschreven. Solotarjow was
zelf ook speler en kende de mogelijkheden van het instrument zeer goed.
Rondo Capriccioso bevat veel thema’s van zijn Sonate nr. 3. De compositie
is opgedragen aan de beroemde accordeonist F. Lips, waar Solotarjow veel
mee samenwerkte. Lips heeft er voor gezorgd dat veel van zijn composities
werden gepubliceerd. Op 33-jarige leeftijd maakte deze veelzijdige
componist helaas een einde aan zijn leven.

### Sometimes - Ad Wammes
Sometimes is speciaal geschreven om het 60-jarig bestaan van het
Haarlems Accordeonorkest te vieren. De drie delen geven weer wat zich in
de geschiedenis van een orkest door de jaren heen kan afspelen:
1. Sometimes, times were good
2. Sometimes, times were sad
3. But most of the time, times were great.

De twee snelle delen hebben een stevige baslijn in pop/rock and roll stijl.
Het middelste deel is rustig en meditatief. Een eerdere compositie van Ad
Wammes, Boardwalk, is door het HAO opgenomen op haar tweede cd
Promenade. Ad Wammes studeerde compositie, piano en elektronische
muziek. Hij heeft muziek geschreven voor TeleacNOT en richt zich sinds
2005 helemaal op de klassieke muziek, met veel aandacht voor het orgel.
De compositie Miroir betekende zijn internationale doorbraak. Het wordt
over de hele wereld gespeeld door toporganisten als Thomas Trotter en
John Scott en verscheen reeds 17x op cd. Zie voor meer informatie zie
[adwammes.nl](www.adwammes.nl).

### Scherzino - Antonín Devátý
De Tsjechische componist Devátý studeerde aan het conservatorium in
Praag. Hij was een allround musicus, componist, violist, organist, dirigent
en docent aan het conservatorium. Hij schreef tonaal, maar gebruikte
soms ook 12-toonstechnieken. Scherzino is een kort, levendig, snel
muziekstuk, vooral in ¾ maatsoort. Het is origineel geschreven voor
accordeonorkest. Het Scherzino heeft een contrasterend middendeel, het
werk heeft de A-B-A vorm.

### Zigeunerweisen - Pablo de Sarasate (arr. W. Maurer)
Pablo de Sarasate (1844-1908) was een Spaans violist en componist die als
vijfjarige met vioollessen begon en al als twaalfjarige aan het hof van
Madrid speelde. Het wonderkind studeerde van 1856 tot 1859 aan het
pretentieuze conservatorium van Parijs. Hij was stellig de grootste violist
van zijn tijd en misschien wel de opvolger van duivelskunstenaar Niccolo
Paganini (1782-1840). Zijn technisch volmaakte spel zat barstensvol Spaans
temperament. De zeer virtuoze Zigeunerweisen uit 1878 is een voorbeeld
van het vakmanschap en kunde van Sarasate als componist.  Het is een
bewerking van volksliedjes en volksdansen uit de Balkan, met Spaans
temperament overgoten.

### Stukken uitgevoerd door Van Amsterdam Duo

#### Rapsodia española al estilo flamenco - Eddy Flecijn
Het werk Rapsodia española al estilo flamenco schreef Flecijn voor zijn
dochter, de violiste Mart, in de zigeuner/flamenco stijl die zij het liefste
speelt. Het werk is origineel geschreven voor accordeon en viool.
Het werk begint met een solo voor de accordeon, die de duistere nacht
verklankt waarin de zigeuners samentroepen. Hierna volgt een solo voor
de viool, vervolgens gaan zij gezamenlijk verder. Bij het laatste gedeelte
van het stuk imiteert de accordeon de dansers die om de beurt klappen in
zeer snel tempo, dit met scherpe akkoorden.
Eddy Flecijn studeerde aan het Koninklijk Vlaams Muziekconservatorium te
Antwerpen accordeon, harmonie, contrapunt en fuga. Hij is nu
accordeonist/ bandoneonist in het sextet Orquesta Atipica en het trio
Vision-On. Voor zijn composities behaalde hij diverse prijzen.

#### Après un rêve - Gabriel Fauré
Gabriel Fauré was een leerling van Saint-Saëns die zich vooral richtte op
het componeren van kamermuziek. Zijn muziek wordt gekenmerkt door
ongewone harmonieën en modulaties. Hij is beroemd geworden met
composities als de Pavane, Dolly Suite, Requiem en zijn vroege compositie
Après un rêve, voor piano en viool (of zangstem). Dit stuk gaat over een
meisje dat droomt van een romantische vlucht met haar verloofde, weg
van de aarde, naar het licht. Maar als ze wakker wordt is ze weer in de
harde werkelijkheid, en ze verlangt terug naar de droom.

#### Danse Macabre - Camille Saint-Saëns
Danse Macabre (dodendans) is een symfonisch gedicht van de Franse
componist Camille Saint-Saëns (1835-1921). De compositie opent met
twaalf middernachtelijke klokslagen. De opmaat door de bassen,
introduceert de Dood die de gestorvenen uit hun graven wekt. De dood
wordt weergegeven in de melodie met een verminderde kwint, ook wel
het duivelsinterval genoemd. In de verbeelding komen meer en meer
doden uit hun graven en beginnen met elkaar te dansen. Steeds voller en
heftiger klinkt de melodie, alsof de skeletten door de wind worden
opgejaagd, in het besef dat ze voor zonsopkomst teruggekeerd moeten
zijn in hun graven. Wanneer op het hoogtepunt uiteindelijk de haan kraait,
gespeeld in de eerste stem, is de dans abrupt voorbij en horen we de
grafstenen weer dichtvallen. De dood blijft als laatste achter, zittend op
een grafzerk, en speelt zijn afscheidsmelodie.

### Fuga Y Misterioso - Astor Piazzolla (arr. R. Baas)
Astor Piazzolla (1921-1992) was een Argentijns tangomuzikant,
bandoneonist en componist. Hij introduceerde de Tango Nuevo, gebaseerd
op de klassieke tango, maar met langere en meer complexe composities
en tempowisselingen. De compositie Fuga Y Misterioso begint met een
fugagedeelte, waarin de eerste stem begint met het thema dat de andere
stemmen herhalen. Daarna volgt een langzamer en geheimzinniger
gedeelte, gevolgd door een spetterend slot.

### La Campanella - Rudolf Würthner
Rudolf Würthner stichtte in 1947 het legendarische Hohner-orkest. Zijn
composities en arrangementen zijn toonaangevend voor accordeon en
accordeonorkest.
La Campanella is een concert-etude, gebaseerd op het gelijknamige stuk
van de componist N. Paganini. Würthner schreef de compositie voor
accordeonsolo en arrangeerde de versie voor orkest en solo-accordeon die
het Haarlems Accordeonorkest speelt.

### Danzón nr. 2 - Arturo Márquez (arr. W. Pfeffer)
De dansstijl danzón komt uit Cuba en is zeer aanwezig in de Mexicaanse
folklore. Het stuk Danzón nr. 2 is één van de meest populaire Mexicaanse
hedendaagse orkestwerken van de prominente componist Arturo
Marquez. De compositie is bekend geworden door het Simón Bolívar
jeugdorkest van Venezuela o.l.v. de wereldberoemde dirigent Gustavo
Dudamel.

## Orkest bezetting 2019
<b>1e stem</b>:
Leo van Lierop, Ron Paeper, Joëlle Dek, Emma Vermeulen, Tim Wagelaar.

<b>2e stem</b>:
Maaike Lips, Jolijn Bakker, Marian Belderok, Simone Visser, Tanja van Opzeeland, Joke van Straten.

<b>3e stem</b>:
Priy Werry, Nina Reehorst, Thijs Paeper, Carla Vermeren, Reinout Fonk, Rikie Paeper, Simone Vogel.

<b>4e stem</b>:
Issa Hanou, Floor de Mulder, Hans van Roode, Margreet de Haas, Petra van Bremen.

<b>Piano/keyboard</b>:
Stefan Rasch.

<b>Basaccordeon</b>:
Gilles Schuringa.

<b>Slagwerk</b>:
Mark Meurs.

<b>Dirigent</b>:
Evert van Amsterdam

## De Dirigent
Evert van Amsterdam studeerde hoofdvak Accrodeon aan het Utrechts Conservatorium bij Miny Dekkers.
Hij speelde in diverse kamermuziekensembles, waaronder D'Accord, Trio Strano, D'Acht en orkest M. Scarlat, momenteel speelt hij in het tango kwartet Cuarteto Renuevo.

Sinds 1984 heeft hij als docent een grote accordeonschool opgebouwd. Veel van de huidige spelers van het HAO zijn daar opgeleid. 
Hij is een van de organisatoren van het jaarlijks Nationaal Accordeon Concours, vanaf 2016 is hij voorzitter van de NOVAM.


## Het Van Amsterdam Duo
Het Van Amsterdam Duo vormt een bijzondere combinatie van de instrumenten accordeo en viool.
Op hun repertoire staan klassieke werken van o.a. Bach en Vivaldi, naast meer hedendaagse componisten zoals Kusjakow en Piazzolla.
In 2011 is hun eerste CD onder naam "A Flight Beyond Time" uitgebracht. In april 2016 brachten zij een Russische CD uit genaamd "Chanson Russe".

Het Duo was laureaat van diverse concoursen in binnen- en buitenland, zoals het Prinses Christina Concours en het prestigieuze Internationaler Akkordeonwettbewerb in Klingenthal.
Voor meer informatie zie [www.vanamsterdamduo.com](www.vanamsterdamduo.com).

<b>Jeanine van Amsterdam</b> (1992) is na het Conservatorium van Amsterdam afgestudeerd als master bij Vera Beths. 
Vanaf 2016 speelt zij 2e viool tutti bij het Nederlands Philharmonisch Orkest.
Jeanine heeft kamermuziekconcerten gegeven met o.a. het "Engadin Kwartet" en het strijkorkest Ciconia Consort. 
Zij heeft meermalen meegespeeld in het Nationaal Jeugd Orkest, in 2012 en 2013 als concertmeester.
Ook speelde zij als solist met het Noord-Hollands Promenade Orkest o.l.v. P. Stam.

<b>Vincent van Amsterdam</b> (1989), winnaar van de Dutch Classical Talent Award 2016, is een groot promotor van het klassiek accordeon. 
Hij heeft zijn Master studie aan de Fontys Hogeschoool voor de Kunsten te Tilburg cum laude afgesloten. 
Vincent is laureaat van diverse concoursen, zoals het Prinses Christina Concours en de Accordion Coupe Mondiale in Kroatië (2010).
Sinds kort is hij docent bij Hart Muziekschool in Haarlem en hoofdvakdocent aan het Fontys Conservatorium in Tilburg. 
Vincent heeft twee solo CD's uitgebracht en zal in 2019 een CD uitbrengen met de gehele Goldberg Variaties van J.S. Bach.

   