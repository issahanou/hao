# Accordeonlessen
Accordeonlessen voor beginners en gevorderden voor kinderen en volwassenen.

Het accordeon is een instrument dat zich de laatste tijd sterk heeft ontwikkeld. 
Je vindt het terug in de klassieke- en lichte muziek, dansmuziek (tango) en volksmuziek (klezmer).

Vanaf heden is het ook mogelijk om je via het Haarlems Accordeonorkest in te schrijven voor een korte cursus accordeon van 4 lessen of voor een gratis proefles accordeon. Na de cursus wordt met de leerling gezocht naar de beste manier om de lessen te vervolgen. De (proef)lessen zijn geschikt voor alle leeftijden en worden gegeven door gediplomeerde docenten accordeon.

In de lessen worden de basisvaardigheden van het instrument geleerd. Er wordt voor de cursus een accordeon van de vereniging ter beschikking gesteld.

Lesdag: in overleg<Br>
Leeftijd: vanaf 7 jaar<Br>
Lesduur: afhankelijk van het aantal deelnemers (1= 30 min, 2= 40 min, 3= 60 min)<Br>
Prijs proefles: gratis<Br>
Prijs korte cursus: € 75,- (inclusief instrument)<Br>

Voor het aanmelden voor de korte cursus of een gratis proefles kunt u een e-mail sturen naar het bestuur: 
[haarlemsaccordeonorkest@gmail.com](mailto:haarlemsaccordeonorkest@gmail.com).
