# Het Orkest

Het Haarlems Accordeon Orkest (HAO) behoort tot één van de beste accordeonorkesten van Nederland. Wat dit orkest zo uniek maakt is de relatief lage gemiddelde leeftijd. Het orkest heeft op dit moment 23 leden (jongvolwassenen en volwassenen). Veel van de huidige spelers van het HAO hebben hun opleiding genoten bij Hart Muziekschool Haarlem (voorheen muziekcentrum Zuid-Kennemerland).

In 2020 heeft Evert van Amsterdam het dirigeerstokje overgedragen aan Leo van Lierop, die op dat moment al tien jaar als speler bij het orkest actief was. Leo  heeft zowel een master Klassiek Accordeon als een bachelor HaFa-Directie voltooid, [lees hier meer over onze dirigent](/orkest/dirigent). Het Haarlems Accordeonorkest begeleidde hem in 2015 tijdens een van de werken die hij uitvoerde voor het behalen van zijn master Accordeon.

In de 60 jaar die het orkest bestaat heeft het vele hoogtepunten gekend. Hieronder kunt u lezen hoe dit tot stand is gekomen.


## De Geschiedenis 

Het Haarlems Accordeonorkest (voormalig Accordeonorkest Esmeralda) werd 15 januari 1959 opgericht door Hans van den Bogaard en de toenmalige dirigent Adriaan Oort. In deze eerste jaren was het orkest zeer actief; er werden nationale en internationale concerten gegeven, onder andere tijdens concoursen en uitwisselingen met orkesten uit Zwitserland en Duitsland. Er werd zelfs een keer in Parijs opgetreden.

Het dirigentschap van Adriaan Oort duurde tot 1967, waarna hij werd opgevolgd door Louw Boellaard. Onder zijn leiding werd in 1970 het hoogste aantal punten op een bondsconcours gehaald. Ook werd er opgetreden voor de radio en tijdens het 12,5-jarig jubileum werd het Hohner orkest o.l.v. Rudolf Würthner naar Haarlem gehaald om een concert te geven. De internationale reizen bleven een onderdeel van de agenda; zo werd bijvoorbeeld deelgenomen aan een Muziekfestival in Bonn. 

In 1981 nam Jac Willems de leiding van het orkest over. Onder zijn leiding voerde het orkest een voor die tijd gedurfd repertoire uit, met als hoogtepunt het jubileumconcert ter gelegenheid van het 25-jarig bestaan, waar Partita Piccola van J. Feld werd gespeeld. In 1985 werd Tom Wilmer de nieuwe dirigent van het orkest. Vier jaar later, in 1989, werd hij opgevolgd door de eerste vrouwelijke dirigent die het orkest kende, Jorina Dekker. Jorina ontpopte zich gedurende circa tien jaar tot een vakvrouw, die de sfeer binnen het orkest uitstekend aanvoelde.

Na een zorgvuldige selectieperiode werd in 1998 Evert van Amsterdam aangetrokken als nieuwe dirigent.  Onder zijn leiding heeft het orkest de afgelopen jaren succesvol concerten gegeven met verschillende koren en orkesten. Bij ieder jaarconcert werd een tweede orkest uitgenodigd. Zo werd er samengewerkt met onder andere het Dubbel Blaaskwintet van het Muziekcentrum Zuid-Kennemerland, het koor New Spirit, het Promenade Orkest uit Haarlem, het Haarlems Clarinet Choir, Accordeonorkest Forzando uit Amsterdam, het Spaarndams Koor, het gospelkoor To the Point, het swingkoor Stardust en de twee accordeonorkesten van Muziekcentrum Zuid-Kennemerland: Air and Tune en de Kennemer Accordeongroep. 

Door de samenwerking met andere orkesten is de accordeon als instrument onder de aandacht van een nog breder publiek gebracht. Ook werd drie keer afgereisd naar Innsbruck voor deelname aan het Wereld muziekfestival. Er werden diverse cd's opgenomen (zie pagina cd). In 2009 werd het 50-jarig jubileum gevierd met een uitverkocht concert in de Philharmonie in Haarlem. In 2017 en 2018 behaalde het orkest een 1e prijs in de hoogste afdeling van het Nationaal Accordeonfestival van de Novam in Ede. Het 60-jarig jubileum in 2019 werd gevierd met een dubbelconcert met vele solisten en uitvoering van het werk “Sometimes”,  dat componist Ad Wammes speciaal voor deze gelegenheid componeerde.


## Naamsverandering

Het orkest is opgericht als Accordeonorkest 'Esmeralda'. In 2014 heeft het orkest besloten haar naam te veranderen in
‘Haarlems Accordeonorkest’. Het orkest heeft de afgelopen jaren een enorme groei doorgemaakt
en vond dat de oude naam niet meer paste bij de moderne en professionelere uitstraling 
en identiteit van het orkest. Met de nieuwe naam wil het orkest ook de duidelijke link 
met Haarlem benadrukken, waar de oorsprong van het orkest ligt.

Toenmalig dirigent Evert van Amsterdam: “Dit accordeonorkest is al jaren een begrip in de 
Nederlandse accordeonwereld. Het orkest onderscheidt zich door het programmeren van een 
grote verscheidenheid aan muziekstijlen. Ik kijk met plezier terug op de afgelopen jaren.
De nieuwe naam ‘Haarlems Accordeonorkest’ past beter bij de uitstraling van het huidige orkest.”