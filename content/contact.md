## Boek ons!

Wilt u een concert met of van het Haarlems Accordeonorkest, neem dan contact op met een van de volgende bestuursleden:

| | |
| ---- | ---- |
Voorzitter | Ron Paeper ([06 50293379](tel:0650293379))
Secretaris | Joëlle Dek
Penningmeester | Priy Werry
Algemeen Bestuurslid | Simone Visser
Algemeen Bestuurslid | Issa Hanou

Ook te bereiken op [haarlemsaccordeonorkest@gmail.com](mailto:haarlemsaccordeonorkest@gmail.com).

[comment]: <> (## Donateur worden?)

[comment]: <> (Voor minimaal €15,00 wordt u donateur van onze vereniging, daarvoor ontvangt u eenmaal per jaar een gratis toegangskaart voor een concert.)

[comment]: <> (Aanmelden als donateur:)

[comment]: <> (Stuur een e-mail naar [haarlemsaccordeonorkest@gmail.com]&#40;mailto:haarlemsaccordeonorkest@gmail.com&#41;.)

[comment]: <> (U krijgt dan zo snel mogelijk bericht van ons! )

## Lid worden? Leuk!

Bent u geïnteresseerd om te komen spelen bij het Haarlems Accordeonorkest?

Neem dan contact op met het secretariaat:

Joëlle Dek, per e-mail [haarlemsaccordeonorkest@gmail.com](mailto:haarlemsaccordeonorkest@gmail.com)

## Repetities

Wij repeteren elke vrijdagavond van 19:45 uur tot 22:15 uur.

<b>Repetitieruimte</b><Br>
Sint Bavoschool<Br>
Eemstraat 15<Br>
Haarlem<Br>