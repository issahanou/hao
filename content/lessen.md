# Accordeonlessen

Accordeonlessen voor beginners en gevorderden voor zowel kinderen als volwassenen.

Het accordeon is een zeer veelzijdig instrument, waarmee zowel klassieke- als lichte muziek gespeeld kan worden, maar bijvoorbeeld ook dansmuziek (tango) en volksmuziek (klezmer). Vanaf de leeftijd van 6 jaar kan er al les gevolgd worden.

### Schrijf je nu in voor 4 probeerlessen accordeon met gratis leen-accordeon!

Het is mogelijk om accordeonlessen te volgen in Haarlem bij een de onderstaande accordeon-docent.
Evert van Amsterdam: [www.accordeonschoolhaarlem.nl](www.accordeonschoolhaarlem.nl)

