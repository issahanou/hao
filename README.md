# HAO

Nieuw website design voor het Haarlems Accordeon Orkest.

[Klik hier voor de live versie](https://haarlemsaccordeonorkest.nl/)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run generate # generate static project
$ npm run start


```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
