# Website Haarlems Accordeonorkest
Hier vind je informatie over hoe de website werkt, en hoe je dingen aan kan passen. 

Je kan dingen gewoon online aanpassen in de gitlab website. Ga naar de pagina die je wilt aanpassen, kun je `Edit` knop gebruiken en dan `single file`:
![Edit](/static/example_edit.png)
Om een pagina toe te voegen, druk op `+` en dan `New file`;
![Add](/static/example_add.png)
Onderaan komt dan zo'n box te staan, daar kun je nog even iets zetten in "commit message" over wat je hebt gedaan, maar het is niet heel belangrijk. Dan druk je op `commit changes`. Vervolgens krijgt issa een mailtje die dan de website kan updaten met de nieuwe wijziging/toevoeging.
![Commit](/static/example_commit.png)

## Acties
De 4 meest voorkomende acties:
- Nieuw agenda item: voeg een bestand toe aan `content/agenda`: zie de template
- Nieuw nieuws item: voeg een bestand toe aan `content/nieuws`: zie de template
- Fotos toevoegen: voeg de foto's toe in `static/images` en pas aan in `pages/media`, meer info hieronder
- Videos toevoegen: zet de video op YouTube en zet de link in `pages/media`, meer info hieronder

## Structuur
Dit is een overzicht van alle mapjes hier, en welke belangrijk te zijn. Voor vragen, vraag Issa.
- `assets`: dit is code voor de opmaak van de site, hier heb je enige CSS kennis voor nodig en hoeft bijna nooit aangepast te worden
- `components`: dit is code voor de structuur van de site, zoals de menu balk, de footer en de split van inhoud over twee helften van een pagina
- `content`: dit is de belangrijkste folder, hier staat namelijk al onze content, en dit is wat aangepast moet worden in updates, dit is ook het makkelijkst te managen, dit is geschreven in Markdown, wat een text formatting taal is, en heel gemakkelijk is
    - `agenda`: hier kun je een nieuw bestand aan toevoegen voor een nieuw agenda item, dan wordt dat automatisch getoond. Kopieer de template en geef het de juiste naam: `yyyy-mm-dd.md`. Agenda heeft de volgende velden:
      - `title`: titel van het item
      - `subtitle`: extra inforamtie
      - `location`: locatie, vermeldt ook het volledige adres
      - `datum`: datum die wordt laten zien, zorg dat dit het juiste formaat is `yyyy-mm-dd D hh:mm` waar eerst de datum wordt weergeven (dit wordt anders laten zien maar dit format zorgt dat het goed gaat), dan een spatie en de zoveelste dag van de week en als laatste de tijd van aanvang. Bijvoorbeeld: `2024-12-15 07 15:00` voor 15 december 2024, op zondag (07) om 3 uur.
      - `admission`: informatie over de kaartverkoop
      - `image`: leuk om een speciale foto te kiezen, anders wordt een default gebruikt
      - `alt`: omschrijving van de foto
      - `link`: link voor meer informatie, default is `mailto:haarlemsaccordeonorkest@gmail.com` voor meer info, anders kan je een link naar een website hier zetten of `/kaartjes` als je de kaartjes kopen pagina hebt geupdatet. Geen aanhalingstekens nodig
    - `cds`: dit is de informatie voor de cd paginas
    - `concertverslagen`: hier kunnen nieuwe concertverslagen worden toegevoegd, de naam hiervan maakt niet zoveel uit (maar jaar is makkelijk voor naslag). Je kan hier ook foto's in toevoegen, zie `innsbruck-2019` voor voorbeelden
    - `nieuws`: hier kun je een nieuw bestand aan toevoegen voor een nieuws item (de vijf meest recente staan op de home pagina). De naam van een bestand is `yyyy-mm-<naam>.md` waar vooral het jaar en de maand belangrijk zijn voor de volgorde. Kopieer de template. De volgende velden zijn nodig
      - `title`: titel van het item
      - `description`: extra inforamtie
      - `datum`: datum waarop wordt gesorteerd: yyyy-mm
      - `image`: leuk om een speciale foto te kiezen, anders wordt een default gebruikt
      - `alt`: omschrijving van de foto
      - `link`: link voor meer informatie, bijvoorbeeld `/agenda` om naar de agenda te gaan, of `/concertverslagen` als het over een concert gaat met een verslag. Er wordt een knop aangemaakt waar mensen op kunnen klikken. Dit kan ook een externe link zijn, zoals `https://nsao.nl`. Geen aanhalingstekens nodig
      - `linkText`: De tekst in de knop waar je op kan klikken om naar de link te gaan
    - De andere bestanden zijn extra paginas, niet alle worden nog gebruikt, vraag Issa voor meer informatie
- `layouts`: dit is meer code voor de structuur van de site, zoals de menu balk, de footer en de split van inhoud over twee helften van een pagina
- `pages`: dit is de andere belangrijke map, de bestanden hierin worden automatisch omgezet naar paginas op de website. 
  - `concertverslagen` hoeft niet aangepast te worden, voeg alleen `content` toe
  - `media`: zie hieronder
  - `nieuws`: Voeg nieuws toe in `content`, voor speciale paginas, zie `evert-afscheid` als voorbeeld of vraag Issa
  - `orkest`: Deze folder bevat:
    - `cds`: de paginas over cds
    - `dirigent.vue` de pagina over de dirigent, voor aanpassingen ga naar `content/dirigent-leo.md`
    - `index.vue` dit is de pagina als je op Orkest klikt in het menu, voor aanpassingen ga naar `content/het-orkest.md`
    - `leden.vue` dit is de pagina met alle leden per stem, aanpassingen worden in deze pagina doorgevoerd
    - `lessen.vue` dit is de pagina over lessen, voor aanpassignen ga naar `content/lessen.md` 
    - `repertoire.vue` de tabel wordt gegenereerd van `static/data/Muziek archief.csv` daar kunnen aanpassingen worden gedaan door de kolom 'op repetoirelijst site' op 'ja' te zetten
  - `agenda.vue` genereert de `content/agenda` items die in de toekomst liggen
  - `alleconcerten.vue` genereert alle items uit `content/agenda`
  - `contact.vue` voor aanpassingen ga naar `content/contact.md`
  - `kaartjes.vue` pas deze pagina aan om een betaallink online te zetten, zie hieronder
- `static`: hier staat alle media
  - `data` hier staat het bestand voor het muziek archief (voor de repertoire pagina)
  - `drukwerk` hier staan posters en cd covers
  - `icons` alle icons die worden gebruikt (vooral onderin, zoals spotify)
  - `images` dit is de belangrijkst, hier staan images die gebruikt kunnen worden in de website
    - `agenda` vooral foto's die voor de agenda pagina worden gebruikt
    - `algemeent` foto's die ergens algemeen op de website worden gebruikt
    - `fotoboeken` foto's per concert
    - `leden` de foto's voor de leden pagina
    - `nieuws` vooral foto's die worden gebruikt voor de nieuws pagina en de home page
    - `video-placeholders` foto's die worden gebruikt als youtube filmpjes lang duren om te laden
  

## Media
Er zijn vier media paginas: `index` (dit is als je op media klikt met hoogtepunten), `fotoboeken`, `videos` en eentje met alle opnames van het concert van 14 november 2021. De index is de 'Media' pagina, hier staan een mix van fotos en videos. 

Je kan de paginas aanpassen met meer/minder fotos. Deze paginas bestaan uit blokken van `<div class="media-block">`, nu is er om en en om een block die ook class "media-block--background" heeft, dus dan begint het met `<div class="media-block media-block--background">`. In dit block krijg je of een `<div class="image-overview">` of een `<div class="video-showcase video-showcase--multiple">` om een youtube filmpje te laten zien. Je kan ook als eerste regel in de `<div class="media-block">` nog een `<h2 class="media-block-title">Koffieconcert 2021</h2>` toevoegen voor een subtitel.

In een blok met `<div class="image-overview">`, krijg je allemaal blokjes zoals:
```          
<div class="image-item">
  <img src="/images/fotoboeken/2019-jubileum-januari/Jaarconcert_b-17-small.jpg" alt="Jubileumconcert">
  <div class="image-title">Jubileumconcert 2019</div>
</div>
```
Hierin kun je de `src` aanpassen, dit is de link naar de foto, die altijd begint met `/images` en leidt naar de foto's in de `static` folder. Je kan ook de `alt` aanpassen, dit is de omschrijving van de foto (voor als de foto niet laadt). Als laatste de titel onder de foto, die zit op de 3de regel, tussen de `>` en `</div>`, hierboven "Jubileumconcert 2019".

In een blok met `<div class="video-showcase video-showcase--multiple">` kun je meerdere filmpjes toevoegen, met ieder een blok:
```
1  <div class="video-container">
2   <div class="video-placeholder">
3     <iframe class="video-frame" src="https://www.youtube.com/embed/5tgNd7t7nsw"
4            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
5            allowfullscreen></iframe>
6   </div>
7   <div class="video-title-container">
8     <h3 class="video-title">Werziade III</h3>
9     <h3 class="video-title video-subtitle">Fritz Dobler</h3>
10  </div>
11 </div>
```
Ook hier is de `src` belangrijk, deze link kun je vinden in youtube. Hiervoor moet je een speciale link hebben: in het filmpje bij YouTube, klik op 'Delen', dan 'Insluiten' en dan zie je zoiets als hieronder. 

![Embed link kopieren](/static/example_embed_link.png)

Hier kopieer je alleen de link die bij 'src' staat en die zet je in de `src` van je filmpje. Verder kun je de titel toevoegen in regel 8 en de componist in regel 9.

### Videos pagina
Ook hier worden de blokken `<div class="media-block media-block--background">` en `<div class="media-block">` afgewisseld voor de rode en witte blokkken. In elke media block staan nu video containers zoals hierboven beschreven

### Fotoboeken pagina
De fotoboeken pagina gebruikt ook media blocken zoals hierboven beschreven. Het gaat hier alelen net iets anders: op de pagina zelf staat een selectie van foto's, en als je dan erop klikt dan kun je door het hele fotoboek op echte grootte heenklikken. Dus de fotosboeken pagina heeft ook weer `<div class="media-block media-block--background">` en `<div class="media-block">` afgewisseld voor de rode en witte blokkken. Dan heeft zo'n blok eerst de tag `<image-showcase-with-selection>` om het klikken te activeren en daarna volgt eerst een selectie foto's die op de pagina staan (in het `<template v-slot:preview>` blokje), en daarna de template alle met alle fotos's die vergroot worden laten zien. Dat ziet er als volgt uit:
```
<div class="media-block">
  <h2 class="media-block-title">Uitwisseling London Accordion Orchestra</h2>
  <image-showcase-with-selection>
    <template v-slot:preview>
      <div class="image-item">
        <img src="/images/fotoboeken/2024-uitwisseling Londen/01._HAO_en_LAO.jpeg" alt="HAO en LAO">
      </div>
      <div class="image-item">
        <img src="/images/fotoboeken/2024-uitwisseling Londen/05._HAO_en_LAO.jpeg" alt="HAO">
      </div>
    </template>
    <template v-slot:all>
      <div class="image-item">
        <img src="/images/fotoboeken/2024-uitwisseling Londen/02._HAO_en_LAO.jpg>" alt="Tabernacle">
      </div>
      ... Herhaal alle foto's in een image item blokje hier ...
    </template>
  </image-showcase-with-selection>
</div>
```

## Kaartjes
Als je deze pagina wilt gebruiken, leg ik uit wat je hier moet zetten. Verander nooit iets buiten de `<template>` blokken (geen stress, het is altijd weer aan te passen). Als er geen kaartjes te koop zijn, zet dan dit erin:
```
<template>
  <div class="main">
    <div class="text-container">
      <h1>Koop hier uw kaartjes</h1>
      <h2>Er zijn momenteel geen kaartjes te bestellen voor onze concerten.</h2>
    </div>
  </div>
</template>
```
Als er wel kaartjes te koop zijn, gebruik dan deze format.
```
1   <template>
2     <div class="main">
3       <div class="text-container">
4         <h1>Koop hier uw kaartjes</h1>
5           <div class="kaartjes-item">
6             <div class="kaartjes-description">
7                 <h2>Dubbele forte</h2>
8                 <h3>Zaterdag 15 juni 2024, 20:00 uur</h3>
9                 <p><u>10 euro per persoon (incl consumptie)</u></p>
10                <p><em>7 euro voor jeugd</em></p>
11                <p>
12                  Betaal
13                  <a href="https://www.ing.nl/particulier/betaalverzoek/index.html?trxid=NHFTOvig37VCOH6Isj1llRHzvr9Tl55J" target="_blank">hier</a>
14                  (of met de QR code) voor de hoeveelheid tickets die je wil bestellen. 
15                  Het bedrag is aanpasbaar. Voer bijv. €30 in als je 3 tickets wilt kopen.
16                  Vul in de omschrijving ook de naam in voor wie de kaartjes zijn, dan kun je ze makkelijk ophalen bij binnenkomst.
17                </p>
18                <p>
19                  Aan de deur zullen wij de tickets op naam van de rekeninghouder overhandigen!
20                </p>
21              <div class="kaartjes-qr">
22                <img class="content-image" src="/drukwerk/betaalqr.jpg" alt="QR">
23              </div>
24              <p>
25                <a href="https://www.ing.nl/particulier/betaalverzoek/index.html?trxid=NHFTOvig37VCOH6Isj1llRHzvr9Tl55J" target="_blank">
26                  https://www.ing.nl/particulier/betaalverzoek/index.html?trxid=NHFTOvig37VCOH6Isj1llRHzvr9Tl55J2
27                </a>
28              </p>
29            </div>
30            <div class="kaartjes-poster">
31              <img class="content-image" src="/drukwerk/Uitwisselingsconcert8-10-22.png" alt="Poster">
32            </div>
33          </div>
34      </div>
35    </div>
36  </template>
```

Deze regels kun je aanpassen met informatie
- regel 7: Hier kun je de titel toevoegen tussen de `<h2>` en `</h2>` blokken
- regel 8: Hier kun je de datum zetten, gewoon in tekst
- regel 9: Hier kun je de prijs zetten, tussen de `<p><u>` en `</u></h2>` blokken
- regel 10: Deze regel heeft de kinder prijs of verwijder deze als er maar 1 prijs is
- regel 15: Deze regel geeft een voorbeeld voor het optellen van prijzen

De betaallink komt op drie plekken:
- regel 13 in `href="hier de link plakken"`, denk aan de "
- regel 24 in `href="hier de link plakken"`, denk aan de "
- regel 26, hier gewoon de link plakken (dan kunnen mensen kopieren)

Als laatste, de QR code staat in `static/drukwerk/betaalqr.jpg` dit bestand kun je aanpassen, en het weer dezelfde naam geven dan wordt deze gelijk geladen. Anders kun je naam aanpassen in regel 22.

De poster wordt ook geladen, pas de link aan in regel 31, in het `src` veld. 

### Kaartjes template en script
De `kaartjes.vue` file bestaat uit twee delen, het eerste deel geeft de content weer en zit 'gevangen' tussen `<template>` and `</template>`. Dit deel pas je aan voor met de kaartjes informatie. Daaronder zit nog een deel voor de styling, dat zit tussen `<script>` en `</script>`. Deze moet altijd blijven staan voor de goede opmaak van de website. Mocht je het per ongeluk toch weghalen, dan staat hieronder het stukje code dat daar hoort te staan.
```
<script>
import Split from "../components/split";

export default {
  async asyncData({$content, params}) {
  },

  components: {
    'split-content': Split,
  }
}
</script>
```