export default {
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
        }
    },
    css: ['~/assets/css/main'],
    dev: process.env.NODE_ENV !== 'production',
    env: {
        baseUrl: process.env.BASE_URL
    },
	server: {
        port: 80, //default:3000
        host: '128.199.53.101'
    },
    head: {
        title: 'Haarlems Accordeonorkest',
        meta: [
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {charset: 'utf-8'},
            {
                hid: 'description',
                name: 'description',
                content:
                    'Het Haarlems Accordeon Orkest bestaat sinds 1959 en behoort tot een van de beste accordeonorkest van Nederland. Het is een groot en divers orkest, met leden van alle leeftijden.',
            }
        ]

    },
    link: [
        {
            rel: 'icon',
            type: 'image/png',
            href: '/favicon.ico'
        },
        {
            rel: 'stylesheet',
            href: 'https://fonts.googleapis.com/icon?family=Material+Icons&display=swap',
        },
    ],
    modules: [
        'nuxt-material-design-icons',
        '@nuxt/content'
    ],
    content: {
        markdown: {
            remarkPlugins: [
                'remark-squeeze-paragraphs',
                'remark-slug',
                'remark-autolink-headings',
                'remark-footnotes'
            ]
        }
    },
    target: 'static',
    buildModules: [
        [
            '@nuxtjs/google-analytics',
            {
                'id': 'UA-180222059-2'
            }
        ],
		[
			'@nuxtjs/google-gtag',
			{
				'id': 'G-385871416',
				'debug': false
			}
		]
    ]
}
