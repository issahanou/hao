## Update website

SSH to the Digital Ocean container

```
$ cd /
$ cd /var/www/html/haarlemsaccordeonorkest-site/
$ git pull
$ npm run build
$ npm run generate
$ sudo nginx -s reload
```


## Reissue SSL

0. Renew SSL certificate in namecheap
1. Generate CSR in `~` (`root`) of server by
```
openssl req -newkey rsa:2048 -nodes -keyout haarlemsaccordeonorkest.nl.key -out haarlemsaccordeonorkest.nl.csr
```
The most important part is the Common Name field, which should match the name that you want to use your certificate with: [haarlemsaccordeonorkest.nl]()

[More info here](https://www.digitalocean.com/community/tutorials/how-to-install-an-ssl-certificate-from-a-commercial-certificate-authority)

2. Click activate button in Namecheap
3. Enter `haarlemsaccordeonorkest.nl.csr`
4. Follow instructions for validation (can be through email)
5. The certificates will be emailed

Use filezilla to transfer the files: port 22 (default)

## SSL configs

```
cd /
```
1. Can upload SSL certificates in a new folder in `/etc/ssl/hao<year>`
   - Can copy file contents in vim using `:%y+`
2. Copy the key from the `/root` directory where you created the CSR request, to the new `/etc/ssl/hao<year>`
3. Copy the `intermediate.crt` file from `etc/ssl/haarlemsaccordeonorkest.nl/` naar `/etc/ssl/hao<year>`
4. Create the new chained certificate:
```
cat haarlemsaccordeonorkest.nl.crt intermediate.crt > haarlemsaccordeonorekst.nl.chained.crt
```
1. Then update the pointer in `/etc/nginx/sites-enabled/haarlemsaccordeonorkest.nl`
2. Restart
```
sudo nginx -s reload
sudo systemctl restart nginx
```


Old:
- Nginx conf in `/etc/nginx/nginx.conf`
- More in `etc/nginx/sites-enabled/haarlemsaccordeonorkest.nl`
- SSL certificates in `/etc/ssl/hao22`
    - New certificates should be uploaded here